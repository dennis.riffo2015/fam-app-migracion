**Proyección de riesgo de caídas con análisis de movimiento en React-Native con Tensorflow.**

El proyecto cuenta con Inteligencia Artificial como herramienta para captura de articulaciones del cuerpo humano y así comparar movimientos con una serie de ejercicios, de los cuales logramos determinar si el sujeto haciendo el test, tiene problemas de equilibrio, entre otros.

Para Inicializar el proyecto debemos tener las siguientes herramientas:

-Android Studio

En terminal ubuntu / powershell windows:
- react-native-cli: 2.0.1
- react-native: 0.59.10
- adb (programa para conexión usb)


Para instalar los componentes vamos a la carpeta application/ y en terminal instalamos las librerías:

_npm install_

Para iniciar la aplicación debemos instalarla en nuestro celular, para visualizar el contenido como desarrollador, debemos abrir el proyecto application/android en Android studio y de ese modo se genera un gradle que compila la apk disponible para android.

Teniendo instalado adb y conectado nuestro dispositivo movil, debemos escribir en nuestra terminal:

_adb devices_

De ese modo obtenemos una id que corresponde a nuestro dispositivo:

_List of devices attached
7UF4C19410000555        device_


Copiamos esa id y en otra linea se escribe lo siguiente:

_adb -s 7UF4C19410000555 reverse tcp:8081 tcp:8081_

Ahora en android studio clickeamos en run 'app'.

Finalmente para visualizar y crear cambios en la aplicación debemos abrir la terminal donde tenemos react-native y escribimos lo siguiente: 

_react-native start --reset-cache_

De esta manera en nuestro dispositivo se pueden visualizar los cambios siempre y cuando se recargue la aplicación, para ello sólo agitamos el celular y apretamos el botón reload.

