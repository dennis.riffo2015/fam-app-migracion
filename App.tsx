import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, Platform, BackHandler, Alert, TouchableOpacity } from 'react-native';
//import DeviceInfo from 'react-native-device-info';

import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-react-native';
import StaticServer from 'react-native-static-server';
import RNFS from 'react-native-fs';
GLOBAL = require('global');

import { Diagnostic } from './components/diagnostic';

//var httpServer = require('react-native-http-server');


const BACKEND_TO_USE = 'rn-webgl';

export type Screen = 'main' | 'diag' ;

interface AppState {
  isTfReady: boolean;
  currentScreen: Screen;
  nombre: String;
}

export class App extends React.Component<{}, AppState> {
  state = {
    url: "127.0.0.1",
  };
  constructor(props: {}) {
    super(props);
    this.state = {
      isTfReady: false,
      currentScreen: 'main',
      nombre: ''
    };

    this.showDiagnosticScreen = this.showDiagnosticScreen.bind(this);
    this.showMainScreen = this.showMainScreen.bind(this);

  }

  getPath() {
    return Platform.OS === "android"
      ? RNFS.DocumentDirectoryPath + "/www"
      : RNFS.MainBundlePath + "/www";
  }

  downloadPath(file){
    RNFS.downloadFile({
        fromUrl: 'https://vjs.cl/apk/'+file,
        toFile: RNFS.DocumentDirectoryPath+'/'+file,
    }).promise.then((r) => {
        this.setState({ isDone: true })
        });
  }

   async moveAndroidFiles() {
     if (Platform.OS === "ios") {
       //MobileNet Directorio
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/mobilenet");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/mobilenet/float");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/mobilenet/float/050");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/mobilenet/float/075");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/mobilenet/float/100");
       //Resnet Directorio
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/resnet50");
       await RNFS.mkdir(RNFS.DocumentDirectoryPath + "/www/models/resnet50/quant2");

       const files = [
         "www/models/mobilenet/float/050/group1-shard1of2.bin",
         "www/models/mobilenet/float/050/group1-shard2of2.bin",
         "www/models/mobilenet/float/050/model-stride16.json",
         "www/models/mobilenet/float/075/group1-shard1of2.bin",
         "www/models/mobilenet/float/075/group1-shard2of2.bin",
         "www/models/mobilenet/float/075/model-stride16.json",
         "www/models/mobilenet/float/100/group1-shard1of2.bin",
         "www/models/mobilenet/float/100/group1-shard2of2.bin",
         "www/models/mobilenet/float/100/model-stride16.json",
         //Resnet bin-json
         "www/models/resnet50/quant2/group1-shard1of12.bin",
         "www/models/resnet50/quant2/group1-shard2of12.bin",
         "www/models/resnet50/quant2/group1-shard3of12.bin",
         "www/models/resnet50/quant2/group1-shard4of12.bin",
         "www/models/resnet50/quant2/group1-shard5of12.bin",
         "www/models/resnet50/quant2/group1-shard6of12.bin",
         "www/models/resnet50/quant2/group1-shard7of12.bin",
         "www/models/resnet50/quant2/group1-shard8of12.bin",
         "www/models/resnet50/quant2/group1-shard9of12.bin",
         "www/models/resnet50/quant2/group1-shard10of12.bin",
         "www/models/resnet50/quant2/group1-shard11of12.bin",
         "www/models/resnet50/quant2/group1-shard12of12.bin",
         "www/models/resnet50/quant2/model-stride16.json",
       ];

       await files.forEach(async file => {
         //console.log("ESTE: "+RNFS.DocumentDirectoryPath);
         console.log(file);
         if (await RNFS.exists(RNFS.DocumentDirectoryPath+'/'+file)){
             console.log("BLAH EXISTS");
         } else {
             this.downloadPath(file)
         }
       });
     }

   }


 
  repositorio(){
        console.log("Estoyaqui"+RNFS.LibraryDirectoryPath)
        }
 
  async componentDidMount() {
    await tf.setBackend(BACKEND_TO_USE);
    await tf.ready();
    await this.moveAndroidFiles();
    let path = getPath();
    //console.log(path);
    this.server = new StaticServer(8080, path, {localOnly : true }); //  -
    this.server.start().then(url => {
    this.setState({ url });
    });
  }

componentWillUnmount() {
  if (this.server && this.server.isRunning()) {
     this.server.stop();
   }
}


   showDiagnosticScreen() {
     this.setState({ currentScreen: 'diag' });
   }

  showMainScreen() {
    this.setState({ currentScreen: 'main' });
  }


  renderMainScreen() {
    //this.Stats()
    return <Fragment>
      <View style={styles.sectionContainer}>
      <Image
                source={require('./assets/images/logo.png')}
                style={styles.logo}
      />
      <TextInput
                placeholder='Nombre Apellido'
                leftIcon={{ type: 'Feather', name: 'mail' }}
                onChangeText={(value) => GLOBAL.USERNAME = value}
                style={styles.input}
                />

        <TouchableOpacity
        style={styles.button}
        onPress={this.showDiagnosticScreen}>
            <Text style={styles.text}>Ingresar</Text>
        </TouchableOpacity>
      </View>
    </Fragment>;
  }

  renderDiagnosticScreen() {
    return <Fragment>
      <Diagnostic returnToMain={this.showMainScreen} />
    </Fragment>;
  }


  renderContent() {
    const { currentScreen, isTfReady } = this.state;
    if (true) {
      switch (currentScreen) {
        case 'main':
          return this.renderMainScreen();
        case 'diag':
           return this.renderDiagnosticScreen();
        case 'body':
          return this.renderBodyrender();
        default:
          return this.renderMainScreen();
      }
    }

  }

//        <Text>{this.state.url}</Text>


  render() {
    return (
      <Fragment>
      <StatusBar barStyle='dark-content' />
      <SafeAreaView>

        <View style={styles.body}>
          {this.renderContent()}
        </View>
      </SafeAreaView>
    </Fragment>
    );
  }


}

function getPath() {
  return Platform.OS === "android"
    ? RNFS.DocumentDirectoryPath + "/assets"
     : RNFS.MainBundlePath + "/assets";
}

const backAction = () => {
      Alert.alert("Salir", "¿Está seguro que desea salir?", [
        {
          text: "Cancelar",
          onPress: () => null,
          style: "cancel"
        },
        { text: "Si", onPress: () => BackHandler.exitApp()
              //this.renderContent();
        }
      ]);
      //
      return true;
    };

const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

//    return () => backHandler.remove();



const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#00aaff',
    padding: '3%',
    margin: '3%',
    borderRadius: 10,
  },
  scrollView: {
    backgroundColor: 'white',
  },
  body: {
    backgroundColor: 'white',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,

  },
  input: {
    borderColor: '#00aaff',
    borderWidth: 1,
    marginBottom: 30,
    marginTop: 30
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'black',
    marginBottom: 6,
  },
  logo: {
    width: 200,
    height: 200,
    alignSelf:'center',
    margin:'5%'
  },
  text: {
    textAlign: 'center',
    alignSelf: "center",
      alignItems: "center",
      color: 'white',
      fontSize: 22,
  },
});
