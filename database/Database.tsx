import SQLite, { SQLiteDatabase } from 'react-native-sqlite-storage';
GLOBAL = require('../components/global');

interface TestUser{
    id_test : number,
    frames : string,
    second_sensor : string,
    operating_system : string,
    timestamp : number
}

interface Test{
    id : number
}


const db = SQLite.openDatabase(
    {
        name: 'fondef.db',
        
        createFromLocation: 1
    },
    () => {
        console.log('[sucess] Connection to database')
    },
    error => {
        console.log('[error] Connection to database:',error);
    }
)
function getTests(){
    let datos = []
    // LISTADO DE TEST
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
            let sql = 'SELECT id_test,name,instructions,duration FROM tests'
            tx.executeSql(sql, [], (tx, resulst) =>{
                console.log('[sucess] getTests: total',resulst.rows.length);
                for (let index = 0; index < resulst.rows.length; index++) {
                    datos.push(resulst.rows.item(index))
                    //console.log(resulst.rows.item(index))
                }
                //console.log(datos);
                resolve(datos);
            },error => {
                console.log('[error] getTests:',error);
                resolve(datos)
            })
        })
    })
}

function getOneTest(testsChecked: String){
    let datos = []
    //console.log('TEST ID' + testsChecked)
    // LISTADO DE TEST
    return new Promise((resolve, reject) =>{
      db.transaction( tx => {
              //console.log('TEST ID' + testChecked)
              let sql = 'SELECT id_test,name,instructions,duration, demo_url FROM tests where id_test = ' + testsChecked
              tx.executeSql(sql, [], (tx, resulst) =>{
                  //console.log('[sucess] getTests: total',resulst.rows.length);
                  for (let index = 0; index < resulst.rows.length; index++) {
                      datos.push(resulst.rows.item(index));
                      //console.log(datos);
                  }
                  resolve(datos);
              },error => {
                  console.log('[error] getTests:',error);
                  resolve(datos)
              });
      })
        })
    }

function getTestsUser(){
    let datos = []
    // LISTADO DE TEST
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
            let sql = 'SELECT tu.id_test_user,tu.id_test,tu.frames,tu.second_sensor,tu.timestamp,tu.sync,t.name AS test, t.duration FROM tests_users AS tu INNER JOIN tests AS t ON t.id_test = tu.id_test'
            tx.executeSql(sql, [], (tx, resulst) =>{
                console.log('[sucess] getTestsUser: total',resulst.rows.length);
                for (let index = 0; index < resulst.rows.length; index++) {
                    datos.push(resulst.rows.item(index))
                    //console.log(resulst.rows.item(index))
                }
                resolve(datos);
            },error => {
                console.log('[error] getTests:',error);
                resolve(datos)
            })
        })
    })
}

function createTestUser(data:TestUser){
    let datos = []
    // LISTADO DE TEST
    GLOBAL.FRAMES = GLOBAL.FRAMES.replace("undefined", "")
    fetch('http://164.77.114.239:5100/Test/AddTest', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         user: GLOBAL.USERNAME,
         frames: "{" + GLOBAL.FRAMES + "}",
         test_number: GLOBAL.TEST_NUMBER,
         test_name: GLOBAL.TEST_NAME
       })
     })
     console.log('http://164.77.114.239:5100/Test/AddTest')
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
            let sql = 'INSERT INTO tests_users (id_test,frames,second_sensor,operating_system,timestamp,sync) VALUES (?,?,?,?,?,0)'
            tx.executeSql(sql,
                [
                    data.id_test,
                    data.frames,
                    data.second_sensor,
                    data.operating_system,
                    data.timestamp
                ], (tx, resulst) =>{
                    if (resulst.rowsAffected > 0) {
                        console.log('[sucess] createTestUser: Successfully registered');
                    }
                    else{
                        console.log('[error] createTestUser: Registration failed');
                    };
                    resolve(datos);
            },error => {
                console.log('[error] createTestUser:',error);
                resolve(datos)
            })
        })
    })
}

function createPlantilla(data:TestUser){
    let datos = []
    // LISTADO DE TEST
    fetch('http://164.77.114.239:5100/Test/AddTest', {
       method: 'POST',
       headers: {
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         user: GLOBAL.USERNAME,
         frames: "{" + GLOBAL.FRAMES + "}",
         //test_number: GLOBAL.TEST_NUMBER,
         test_name: GLOBAL.TEST_NAME
       })
     })
     //console.log(GLOBAL.TEST_NAME)
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
            let sql = 'UPDATE tests set demo_frames = ? where id_test = 3'
            tx.executeSql(sql,
                [
                    data.frames,
                ], (tx, resulst) =>{
                    if (resulst.rowsAffected > 0) {
                        console.log('[sucess] createTestUser: Successfully registered');
                    }
                    else{
                        console.log('[error] createTestUser: Registration failed');
                    };
                    resolve(datos);
            },error => {
                console.log('[error] createTestUser:',error);
                resolve(datos)
            })
        })
    })
}

function getFrames(testsChecked: Object[]){
    let datos = []
    // LISTADO DE TEST
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
            testsChecked.forEach(testID => {
                //console.log(testID + ' TEST ID');
                let sql = 'select id_test,id_test_user,frames from tests_users where id_test_user = ' + testID
                tx.executeSql(sql, [], (tx, resulst) =>{
                    //console.log('[sucess] getTests: total',resulst.rows.length);
                    for (let index = 0; index < resulst.rows.length; index++) {
                        datos.push(resulst.rows.item(index));
                        //console.log(resulst.rows.item(index));
                    }
                    // var data_json = JSON.parse(datos[0]['frames'].toString())
                    // //console.log(data_json)
                    // var frames = Object.keys(data_json)
                    // for (let i = 0; i < Object.keys(data_json).length; i++) {
                    //    console.log(frames[i]);
                    //    console.log(data_json[frames[i]]);
                    // }
                    //console.log(JSON.parse(datos[0]['id_test'].toString()))
                    resolve(datos);
                },error => {
                    console.log('[error] getTests:',error);
                    resolve(datos)
                });
            })
        })
    })
}



function getFrames_plantilla(testsChecked: String){
    let datos = []
    // LISTADO DE Plantilla
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
                console.log(testsChecked);
                let sql = 'select demo_frames from tests where id_test = ' + testsChecked
                tx.executeSql(sql, [], (tx, resulst) =>{
                    console.log('[sucess] getTests: total',resulst.rows.length);
                    for (let index = 0; index < resulst.rows.length; index++) {
                        datos.push(resulst.rows.item(index));
                    }
                     // var data_json = JSON.parse(datos[0]['demo_frames'].toString())
                     // var frames = Object.keys(data_json)
                     // for (let i = 0; i < Object.keys(data_json).length; i++) {
                     //    console.log(frames[i]);
                     //    console.log(data_json[frames[i]]);
                     // }
                    resolve(datos);
                },error => {
                    console.log('[error] getTests:',error);
                    resolve(datos)
                });
        })
    })
}

function getFrames_test(){
    let datos = []
    // LISTADO DE Plantilla
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
                let sql = 'select * from tests_users'
                tx.executeSql(sql, [], (tx, resulst) =>{
                    //console.log('[sucess] getTests: total',resulst.rows.length);
                    for (let index = 0; index < resulst.rows.length; index++) {
                        datos.push(resulst.rows.item(index));
                        console.log(resulst.rows.item(index));
                    }

                   var data_json = JSON.parse(datos[1]['frames'].toString())
                   var frames = Object.keys(data_json)
                   for (let i = 0; i < Object.keys(data_json).length; i++) {
                      console.log(frames[i]);
                      console.log(data_json[frames[i]]);
                   }
                    resolve(datos);
                },error => {
                    console.log('[error] getTests:',error);
                    resolve(datos)
                });
        })
    })
}

function getImages(){
    let datos = []
    // LISTADO DE Plantilla
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
                let sql = 'select * from images'
                tx.executeSql(sql, [], (tx, resulst) =>{
                    console.log('[sucess] getTests: total',resulst.rows.length);
                    for (let index = 0; index < resulst.rows.length; index++) {
                        datos.push(resulst.rows.item(index));
                        //console.log(datos);
                    }
                    resolve(datos);
                },error => {
                    console.log('[error] getTests:',error);
                    resolve(datos)
                });
        })
    })
}

function deleteTests(){
    for (let index = 9; index < 20; index++) {
    db.transaction(
                tx => {
                  tx.executeSql(`delete from tests_users where id_test_user = ?;`, [index]);
                },
                null,
                this.update
              )
            }
}

function deletePlantillas(){
    for (let index = 0; index < 70; index++) {
    db.transaction(
                tx => {
                  tx.executeSql(`delete from test_comparation where id_test = ?;`, [index]);
                },
                null,
                this.update
              )
            }
}

function getCuestionario(){
  let datos = []
  // LISTADO DE Plantilla
  return new Promise((resolve, reject) =>{
      db.transaction( tx => {
              let sql = 'select * from cuestionario'
              tx.executeSql(sql, [], (tx, resulst) =>{
                  //console.log('[sucess] getCuestionario: total',resulst.rows.length);
                  for (let index = 0; index < resulst.rows.length; index++) {
                      datos.push(resulst.rows.item(index));
                  }
                  resolve(datos);
              },error => {
                  console.log('[error] getCuestionario:',error);
                  resolve(datos)
              });
      })
  })
}

function getNames_test(){
    let datos = []
    // LISTADO DE Plantilla
    return new Promise((resolve, reject) =>{
        db.transaction( tx => {
                let sql = 'select name from tests'
                tx.executeSql(sql, [], (tx, resulst) =>{
                    //console.log('[sucess] getTests: total',resulst.rows.length);
                    for (let index = 0; index < resulst.rows.length; index++) {
                        datos.push(resulst.rows.item(index));
                    }
                    resolve(datos);
                },error => {
                    console.log('[error] getTests:',error);
                    resolve(datos)
                });
        })
    })
}

export { getTests , createTestUser , getTestsUser, getFrames, deleteTests, getFrames_plantilla, createPlantilla, getFrames_test, deletePlantillas, getOneTest, getImages, getCuestionario, getNames_test}
