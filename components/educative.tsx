import React, { Fragment} from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity, Picker, Dimensions } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { Diagnostic } from '../components/diagnostic';
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import { Linking } from 'react-native';

const jsonmult = require('./customData.json');

export type Screen = 'main' | 'survey';
let mult:List = [];

interface ScreenProps {
    returnToMain: () => void;
}
interface AppState {
    currentScreen: Screen;
    screenHeight: Text;
}

export class Educative extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'survey',
          screenHeight: Dimensions.get('window').height,
          selected: []
        }
        this.showMainScreen = this.showMainScreen.bind(this);
    };

    async runQuery(){
        //mult = jsonmult;

        mult = Object.keys(jsonmult)
        // mult.map( preg =>  console.log(preg))
    }

    componentDidMount() {
        this.runQuery().then(() => this.setState({ready:true}));
    }

    showMainScreen() {
        //cuestionario = getCuestionario();

        this.setState({ currentScreen: 'main' });
    }

    renderdemovideo(src){
      if (src != ''){
      return <Fragment>
      <Video style={styles.video}
      repeat
      volume={1.0}
          source={{uri: src}}
          onBuffer={this.onBuffer}                // Callback when remote video is buffering
         onError={this.videoError}
          ref={(ref) => {
              this._player = ref
          }}
          />
    </Fragment>
    }
    }

    rendermulti(mult){
       var titulo = jsonmult[mult]['Titulo']
       var contenido = jsonmult[mult]['Contenido']
       var url = jsonmult[mult]['URL'][0]
       var embed = jsonmult[mult]['Embed']
       var ref = jsonmult[mult]['Ref']
    //   console.log(frames)
    //   let data = []
    //   frames.map( preg => {
    //     data.push({value: data_json[preg]})
    // })
    return (
      <View key={mult}>
      <Text style={styles.titlemult}>{titulo}</Text>
      <Text style={styles.description}>{contenido}</Text>

      <Text style={{justifyContent: 'center', alignSelf: 'center', color: 'blue', marginBottom: '5%'}}
            onPress={() => Linking.openURL(url)}>
        {url}
      </Text>
      <Text style={styles.description}>Referencia: {ref}</Text>
      <View>{this.renderdemovideo(embed)}</View>
      </View>
    );
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen}/>
        </Fragment>;
    }





    renderSurvey() {
        //console.log(cuestionario);
        return <Fragment>
            <Text style={styles.title}>Módulo Educativo</Text>
            <View style={{height: '81%'}}>
            <ScrollView>

            {
              //console.log(frames);
              mult.map( preg => this.rendermulti(preg))
            }


            </ScrollView>
            </View>
            <View>
            <TouchableOpacity
            style={styles.button}
            onPress={this.props.returnToMain}>
                <Text style={styles.textButton}>Volver</Text>
            </TouchableOpacity>
            </View>
        </Fragment >
    }
    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        default:
            return this.renderSurvey();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
    },
    video: {
      justifyContent: "center",
      alignItems: "center",
      marginBottom: '100%',
      height: "1%",
      width: "3%"
    },
    container: {
        paddingHorizontal: 10,
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    buttonSend: {
        alignItems: 'center',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    textButton: {
        color: "#FFFFFF",
        fontSize: 18
      },
    title: {
        fontSize: 24,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 10,
        color:'#00aaff',
        fontWeight: 'bold'
    },
    titlemult: {
        borderRadius: 10,
        fontSize: 20,
        backgroundColor: "#00aaff70",
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
        color:'black'
    },
    subtitle: {
        fontSize: 20,
        margin: '1%',
        color:'black'
    },
    description: {
        marginHorizontal: 15,
        fontSize: 18,
        backgroundColor: "white",
        textAlign: "justify",
        color:'black',
        marginBottom:'5%'
    },
    input: {
        borderColor: '#00aaff',
        borderWidth: 1,
        height: '7%'
    },
    picker: {
        borderColor: '#00aaff',
        borderWidth: 1,
        height: '7%',
    },
    text: {
        color: 'white',
        fontSize: 18,
    },
        dropdown: {
            backgroundColor: 'white',
            borderBottomColor: 'gray',
            borderBottomWidth: 0.5,
            marginTop: 20,
        },
        dropdown2: {
            backgroundColor: 'white',
            borderColor: 'gray',
            borderWidth: 0.5,
            marginTop: 20,
            padding: 8,
        },
        icon: {
            marginRight: 5,
            width: 18,
            height: 18,
        },
        item: {
            paddingVertical: 17,
            paddingHorizontal: 4,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        textItem: {
            flex: 1,
            fontSize: 16,
        },
});
