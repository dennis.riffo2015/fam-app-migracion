import React, { Fragment } from "react";
import {
  ActivityIndicator,
  Button,
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  Alert,
  Text,
  TextInput,
} from "react-native";
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';


interface ScreenProps {
    returnToMain: () => void;
    demoURL: Object[];
}

export class Demo extends React.Component<ScreenProps, Props> {

  constructor(props: ScreenProps) {
    super(props);
    this.state = {
      currentScreen: 'tests',
      screen : {
        btnBack:{
          disabled  : false,
          show  : true,
          text : 'Volver',
        }
      }
    }
  };

rendervolver(){
  return <Fragment>
  <TouchableOpacity
  style={styles.button}
  onPress={this.props.returnToMain}>
      <Text style={styles.buttonText}>Volver</Text>
  </TouchableOpacity>
  </Fragment>
}

renderdemovideo(){
  return <Fragment>
  <View>
  <Video style={styles.video}
  repeat
  volume={0.0}
      source={{uri: this.props.demoURL}}
      onBuffer={this.onBuffer}                // Callback when remote video is buffering
     onError={this.videoError}
      ref={(ref) => {
          this._player = ref
      }}
      />

  </View>
  <View>
  {this.rendervolver()}
  </View>
</Fragment>
}

render() {
    return (
      <View>
      <View>
        <Text style={styles.title}>Demostración</Text>
      </View>

        {this.renderdemovideo()}

      </View>
    );
  }
}
//        <Separator />

const styles = StyleSheet.create({
  fixToText: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
video: {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginTop: '30%',
  marginLeft: '20%',
  width: "60%",
  height: "60%"
},
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 1
  },
    container: {
      flex: 1,
      justifyContent: "center",
      paddingHorizontal: 1,
      marginBottom: '5%'
    },
  cameraContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
    title: {
      fontSize: 24,
      justifyContent: "center",
      fontWeight: "bold",
      alignSelf: "center",
      marginTop: 20
    },
    button: {
        alignItems: "center",
        backgroundColor: "#00aaff",
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    buttonText: {
      color: "white",
      fontSize: 18
    },
    counterText: {
      fontSize: 42
    },
      containerButtons: {
        position: "absolute",
        marginTop: "100%",
        zIndex: 1,
        justifyContent: "center",
        alignSelf: "center",
          width: 600 / 2
      }
});
