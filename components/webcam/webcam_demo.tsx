import * as React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image, Alert, Platform } from "react-native";

import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { ExpoWebGLRenderingContext } from 'expo-gl';

import * as tf from '@tensorflow/tfjs';
import * as blazeface from '@tensorflow-models/blazeface';
import * as posenet from '@tensorflow-models/posenet';
import { cameraWithTensors } from '@tensorflow/tfjs-react-native';
//import { RNCamera } from 'react-native-camera';
//import { createTestUser } from '../database/Database';


interface Props {
  navigation: any,
  route: any
}

interface TestUser{
  id_test : number,
  test : string,
  frames : string,
  second_sensor : string,
  operating_system : string,
  timestamp : number
}

interface Screen{
  test : {
    id_test : number,
    name : string,
    duration : number,
    instructions : string
  },
  screen:{
    state : number,
    rec   : boolean,
    btnMain : {
      disabled  : boolean,
      show : boolean,
      text : string
    },
    btnBack : {
      disabled  : boolean,
      show : boolean,
      text : string
    }
  },
  timer : {
    counter : number,
    mood : number, // 0 NADA - 1 PREV - 2 TEST
    state : boolean,
    text : string,
  }
}

let     timer           :NodeJS.Timeout;
const   counterInitial  :number = 5
let     counterTest     :number = 15
export class WebcamDemo extends React.Component<Props,Screen>{
  hasCameraPermission?: boolean;
  cameraType: any;
  test_user: TestUser;
  constructor(props: Props) {
    super(props);
    this.state = {
      test : {
        id_test:        this.props.route.params.id_test,
        name:           this.props.route.params.name,
        duration:       this.props.route.params.duration,
        instructions:   this.props.route.params.instructions
      },
      screen : {
        state : 0,// 0 inicial - 1 iniciado - 2 finalizado - 3 guardando datos - 4 anulado
        rec   : false,// 0 inicial - 1 iniciado - 2 finalizado - 3 guardando datos - 4 anulado
        btnMain:{
          disabled  : false,
          show  : true,
          text : 'Iniciar',
        },
        btnBack:{
          disabled  : false,
          show  : true,
          text : 'Volver a instrucciones',
        }
      },
      timer : {
        counter   : counterInitial,
        mood      : 0,      // prev or test
        state     : false,  // true or false
        text      : ''      // true or false
      }
    };
    counterTest = props.route.params.duration;
  }
  startPrev(){
    this.state.screen.rec = false;
    console.log('Starting Preparation')
    this.updateTimer(1,true,'Prepárate',counterInitial)
    this.btnMainUpdateState(1,'ANULAR', false,true)
    this.btnBackUpdateState(1,'Volver a instrucciones', true,false)
    timer = setInterval(()  => {
      this.disCounter()
    }, 1000);
  }
  startTest(){
    this.state.screen.rec = true;
    clearInterval(timer)
    console.log('Starting Test')
    console.log('Capturing data')
    this.updateTimer(2,true,'Grabando',counterTest)
    this.btnMainUpdateState(1,'ANULAR', false,true)
    this.btnBackUpdateState(1,'Volver a instrucciones', true,false)
    let timestamp             = new Date();
    this.test_user.timestamp  = timestamp.getTime()
    timer = setInterval(()  => {
      this.disCounter()
    }, 1000);
  }
  updateTimer(mood:number,state:boolean,text:string,counter:number){
    this.state.timer.mood     = mood
    this.state.timer.state    = state
    this.state.timer.text     = text
    this.state.timer.counter  = counter
    this.forceUpdate();
  }
  cancelTest(){
    this.state.screen.rec = false;
    console.log('Cancel Test')
    clearInterval(timer)
    this.updateTimer(0,false,'',counterInitial)
    this.btnMainUpdateState(4,'Reintentar', false,true)
    this.btnBackUpdateState(4,'Volver a instrucciones', false,true)
  }
  finishTest(){
    this.state.screen.rec = false;
    console.log('Ending Test')
    clearInterval(timer)
    this.updateTimer(0,false,'',counterInitial)
    this.saveData(
      this.test_user
    );
  }
  saveData(data:TestUser){
    console.log('Saving data...')
    this.btnMainUpdateState(3,'Guardando datos...', true,true)
    this.btnBackUpdateState(3,'Volver a instrucciones', false,false)
    createTestUser(
      data
    );
    setTimeout(() =>{
      this.props.navigation.navigate('TestResult',this.test_user)
      this.btnMainUpdateState(0,'Iniciar', false,true)
      this.btnBackUpdateState(0,'Volver a instrucciones', false,true)
    },2000);
  }
  disCounter = () => {
    let a = 0;
    if(this.state.timer.state == true){
      if(this.state.timer.mood == 1){
        a = 1;
        if(this.state.timer.counter < 0){
          this.startTest()
          return 0
        }
        else{
          this.updateTimer(a,true,this.state.timer.counter.toString(),this.state.timer.counter)
        }
      }
      else if(this.state.timer.mood == 2){
        a = 2;
        if(this.state.timer.counter < 0){
          this.finishTest()
          return 0
        }
        else{
          this.updateTimer(a,true,this.state.timer.counter.toString(),this.state.timer.counter)
        }
      }
      this.state.timer.counter  = this.state.timer.counter - 1
    }
  }

  btnMainUpdateState(state:number , text:string, disabled:boolean, show:boolean){
    this.state.screen.state   = state;
    this.state.screen.btnMain = {
      text      : text,
      disabled  : disabled,
      show      : show
    }
    this.forceUpdate();
  };
  btnBackUpdateState(state:number , text:string, disabled:boolean, show:boolean){
    this.state.screen.state   = state;
    this.state.screen.btnBack = {
      text      : text,
      disabled  : disabled,
      show      : show
    }
  };
  btnMain(states:Screen){
    if(states.screen.state == 0){       // DE INICIAL A INICIAR
      this.startPrev()
    }
    else if(states.screen.state == 1){  // DE INICIADO A ANULADO
      this.cancelTest()
    }
    else if(states.screen.state == 2){  // FINALIZADO A GUARDANDO DATOS
      this.finishTest()
    }
    else if(states.screen.state == 4){  // ANULADO A REPETIR
      this.startPrev()
    }
  }
  componentDidMount(){
    let d = new Date();
    this.test_user = {
      id_test : this.state.test.id_test,
      test    :this.state.test.name,
      frames            : '',
      second_sensor     : '',
      operating_system  : Platform.OS,
      timestamp         : d.getTime()
    }
  }
  componentWillUnmount(){
    clearInterval(timer)
  }
  render(){
    return <>
      <View>
          <Text style={styles.title}>{this.state.test.name}</Text>
      </View>
      <View style={styles.containerCamera}>
      <TensorCamera
        // Standard Camera props
        style={styles.camera}
        type={this.state.cameraType}
        zoom={0}
      />
      </View>
      <View style={styles.containerButtons}>
        {
          this.state.timer.state ?
          <TouchableOpacity
            style={ styles.counter }
            onPress={ () => this.startTest() } disabled={true}>
            <Text style={styles.counterText}>{ this.state.timer.text }</Text>
          </TouchableOpacity> : null
        }
        {
          this.state.screen.btnMain.show ?
          <TouchableOpacity
            style={ styles.button }
            onPress={ () => this.btnMain( this.state ) }
            disabled={ this.state.screen.btnMain.disabled }>
            <Text style={styles.buttonText}>{ this.state.screen.btnMain.text }</Text>
          </TouchableOpacity> : null
        }
        {
          this.state.screen.btnBack.show ?
          <TouchableOpacity
            style={ styles.button }
            onPress={ () => this.props.navigation.navigate('TestInstructions', this.state.test) }
            disabled={ this.state.screen.btnBack.disabled }>
            <Text style={styles.buttonText}>Volver a Instrucciones</Text>
          </TouchableOpacity> : null
        }

      </View>
    </>
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10,
    marginBottom: '5%'
  },
  containerCamera: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
    //paddingHorizontal: 10,
    // marginBottom: '5%',
    width: '100%',
    height: '100%',
    display: 'flex'
  },
  containerButtons: {
    justifyContent: "center",
    alignSelf: "center",
    marginTop: '5%',
    //paddingHorizontal: 10,
    width: '100%',
  },
  camera: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    justifyContent: "center",
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30
  },
  button: {
    alignItems: "center",
    backgroundColor: "#00aaff",
    padding: 10,
    margin: '2%'
  },
  buttonText: {
    color: "white",
    fontSize: 18
  },
  counter: {
    alignItems: "center",
    padding: 10
  },
  counterText: {
    fontSize: 42
  }
});
