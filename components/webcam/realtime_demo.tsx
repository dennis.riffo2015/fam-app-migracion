import React from "react";
import {
  ActivityIndicator,
  Button,
  StyleSheet,
  View,
  TouchableOpacity,
  Platform,
  Alert,
  Text,
  TextInput,
} from "react-native";

import Svg, { Circle, Rect, G, Line } from "react-native-svg";
import * as Permissions from "expo-permissions";
import { Camera } from "expo-camera";
import { ExpoWebGLRenderingContext } from "expo-gl";
import * as tf from "@tensorflow/tfjs";
import * as blazeface from "@tensorflow-models/blazeface";
import * as posenet from "@tensorflow-models/posenet";
//const posenet = require('@tensorflow-models/posenet');
import { cameraWithTensors,asyncStorageIO, bundleResourceIO, fetch } from "@tensorflow/tfjs-react-native";
import SQLite, { SQLiteDatabase } from "react-native-sqlite-storage";
import { createTestUser } from '../../database/Database';
import Tflite from 'tflite-react-native';
import Tts from 'react-native-tts';
import { loadModel } from './models';


let face_data_x: string = "";
let face_data_y: string = "";
let cara_x: string = "";
let cara_y: string = "";
let hombro_izq_x: string = "";
let hombro_izq_y: string = "";
let hombro_der_x: string = "";
let hombro_der_y: string = "";
let codo_izq_x: string = "";
let codo_izq_y: string = "";
let codo_der_x: string = "";
let codo_der_y: string = "";
let muñeca_izq_x: string = "";
let muñeca_izq_y: string = "";
let muñeca_der_x: string = "";
let muñeca_der_y: string = "";
let cadera_izq_x: string = "";
let cadera_izq_y: string = "";
let cadera_der_x: string = "";
let cadera_der_y: string = "";
let rodilla_izq_x: string = "";
let rodilla_izq_y: string = "";
let rodilla_der_x: string = "";
let rodilla_der_y: string = "";
let tobillo_izq_x: string = "";
let tobillo_izq_y: string = "";
let tobillo_der_x: string = "";
let tobillo_der_y: string = "";

let comparation: string = "";
let init_compar: string = "Grabando";


let fps: number = 1;
var maxfps = [0,0,0,0,0,0,0,0,0,0,0];
var avg_fps = 0;
let date_fps: string = Date();
let timestamp_fps: number = 0;

interface ScreenProps {
  returnToMain: () => void;
  testID: Object[];
  testName: Object[];
  testDuration: Object[];
}

interface ScreenState {
  hasCameraPermission?: boolean;
  // tslint:disable-next-line: no-any
  cameraType: any;
  isLoading: boolean;
  posenetModel?: posenet.PoseNet;
  pose?: posenet.Pose;
  // tslint:disable-next-line: no-any
  faceDetector?: any;
  faces?: blazeface.NormalizedFace[];
  modelName: string;
}

interface Screen{
  test : {
    id_test : number,
    name : string,
    duration : number,
    instructions : string

  },
  fps: number,
  screen:{
    state : number,
    rec   : boolean,
    btnMain : {
      disabled  : boolean,
      show : boolean,
      text : string
    },
    btnBack : {
      disabled  : boolean,
      show : boolean,
      text : string
    }
  },
  timer : {
    counter : number,
    mood : number, // 0 NADA - 1 PREV - 2 TEST
    state : boolean,
    text : string,
  }
}


let     timer           :NodeJS.Timeout;
const   counterInitial  :number = 10
let     counterTest     :number = 15
let counter_timestamp  :number = 0
let counter_test  :number = 0

const db = SQLite.openDatabase(
  {
    name: "fondef.db",
    location: "default",
    createFromLocation: 1,
  },
  () => {
    console.log("Conexión a DB exitosa");
  },
  (error) => {
    console.log("Error de conexión a DB: ", error);
  }
);
const inputTensorWidth = 152;
const inputTensorHeight = 200;
const AUTORENDER = true;

// tslint:disable-next-line: variable-name
const TensorCamera = cameraWithTensors(Camera);

export class RealtimeDemo extends React.Component<ScreenProps, ScreenState, Props, Screen> {
  rafID?: number;
  db: SQLiteDatabase;

  constructor(props: ScreenProps) {
    super(props);
    this.state = {
      url:null,
      isLoading: true,
      cameraType: Camera.Constants.Type.front,
      modelName: "posenet",
      test : {
        id_test:        1,
        name:           'Test 1',
        duration:       this.props.testDuration,
        instructions:   'Instrucciones'
      },
      fps: 1,
      screen : {
        state : 0,// 0 inicial - 1 iniciado - 2 finalizado - 3 guardando datos - 4 anulado
        rec   : false,// 0 inicial - 1 iniciado - 2 finalizado - 3 guardando datos - 4 anulado
        btnMain:{
          disabled  : false,
          show  : true,
          text : 'Iniciar',
        },
        btnBack:{
          disabled  : false,
          show  : true,
          text : 'Volver a instrucciones',
        }
      },
      timer : {
        counter   : counterInitial,
        mood      : 0,      // prev or test
        state     : false,  // true or false
        text      : ''      // true or false
      }
    };
    this.handleImageTensorReady = this.handleImageTensorReady.bind(this);
    this.timestamp_fps = 0;
    this.maxfps = [0,0,0,0,0,0,0,0,0,0,0];
    this.avg_fps = 0
    counterTest = this.props.testDuration;
  }



  startPrev(){
    this.state.screen.rec = false;
    console.log('Starting Preparation')
    Tts.speak("Prepárate", {
    androidParams: {
      KEY_PARAM_PAN: -1,
      KEY_PARAM_VOLUME: 0.5,
      KEY_PARAM_STREAM: 'STREAM_MUSIC',
      },
      });
    GLOBAL.FRAMES = "";
    this.updateTimer(1,true,'Prepárate',counterInitial)
    this.btnMainUpdateState(1,'ANULAR', false,true)
    this.btnBackUpdateState(1,'Volver a instrucciones', true,false)
    timer = setInterval(()  => {
      this.disCounter()
    }, 1000);
  }
  startTest(){
    this.state.screen.rec = true;
    clearInterval(timer)
    //console.log('Starting Test')
    //console.log('Capturing data')
    Tts.speak("Grabando", {
    androidParams: {
      KEY_PARAM_PAN: -1,
      KEY_PARAM_VOLUME: 0.5,
      KEY_PARAM_STREAM: 'STREAM_MUSIC',
      },
      });
    this.updateTimer(2,true,'Grabando',counterTest)
    this.btnMainUpdateState(1,'ANULAR', false,true)
    this.btnBackUpdateState(1,'Volver a instrucciones', true,false)
    let timestamp             = new Date();
    counter_timestamp = 0
    this.test_user.timestamp = timestamp.getTime()
    this.test_user.frames = "{"
    timer = setInterval(()  => {
      this.disCounter()
    }, 1000);
  }
  updateTimer(mood:number,state:boolean,text:string,counter:number){
    this.state.timer.mood     = mood
    this.state.timer.state    = state
    this.state.timer.text     = text
    this.state.timer.counter  = counter
    //console.log(this.state.timer.counter.toString());
    this.forceUpdate();
  }
  cancelTest(){
    this.state.screen.rec = false;
    console.log('Cancel Test')
    clearInterval(timer)
    this.updateTimer(0,false,'',counterInitial)
    this.btnMainUpdateState(4,'Reintentar', false,true)
    this.btnBackUpdateState(4,'Volver a instrucciones', false,true)
  }
  finishTest(){
    this.state.screen.rec = false;
    console.log('Ending Test')
    clearInterval(timer)
    this.updateTimer(0,false,'',counterInitial)
    this.saveData(
      this.test_user
    );
  }
  saveData(data:TestUser){
    console.log('Saving data...')
    this.btnMainUpdateState(3,'Guardando datos...', true,true)
    this.btnBackUpdateState(3,'Volver a instrucciones', false,true)
    this.test_user.frames = this.test_user.frames + "}"
    GLOBAL.TEST_NUMBER = counter_test
    createTestUser(
      data
    );
    counter_test++;
    // console.log(this.test_user.frames)
    setTimeout(() =>{
      this.props.returnToMain
      this.btnMainUpdateState(0,'Iniciar', false,true)
      this.btnBackUpdateState(0,'Volver a instrucciones', false,true)
    },2000);
  }
  disCounter = () => {
    let a = 0;
    if(this.state.timer.state == true){
      if(this.state.timer.mood == 1){
        a = 1;
        if(this.state.timer.counter < 0){
          this.startTest()
          return 0
        }
        else{
          this.updateTimer(a,true,this.state.timer.counter.toString(),this.state.timer.counter)
        }
      }
      else if(this.state.timer.mood == 2){
        a = 2;
        if(this.state.timer.counter < 0){
          this.finishTest()
          return 0
        }
        else{
          this.updateTimer(a,true,this.state.timer.counter.toString(),this.state.timer.counter)
        }
      }
      Tts.speak(this.state.timer.counter.toString(), {
      androidParams: {
        KEY_PARAM_PAN: -1,
        KEY_PARAM_VOLUME: 0.5,
        KEY_PARAM_STREAM: 'STREAM_MUSIC',
        },
        });
      this.state.timer.counter  = this.state.timer.counter - 1;
    }
  }

  btnMainUpdateState(state:number , text:string, disabled:boolean, show:boolean){
    this.state.screen.state   = state;
    this.state.screen.btnMain = {
      text      : text,
      disabled  : disabled,
      show      : show
    }
    //console.log(state);
    this.forceUpdate();
  };
  btnBackUpdateState(state:number , text:string, disabled:boolean, show:boolean){
    this.state.screen.state   = state;
    this.state.screen.btnBack = {
      text      : text,
      disabled  : disabled,
      show      : show
    }
  };
  btnMain(states:Screen){
    if(states.screen.state == 0){       // DE INICIAL A INICIAR
      this.startPrev()
    }
    else if(states.screen.state == 1){  // DE INICIADO A ANULADO
      this.cancelTest()
    }
    else if(states.screen.state == 2){  // FINALIZADO A GUARDANDO DATOS
      this.finishTest()
    }
    else if(states.screen.state == 4){  // ANULADO A REPETIR
      this.startPrev()
    }
  }

  componentWillUnmount(){
    clearInterval(timer)
  }


  async loadPosenetModel() {

    // const modelJson2 = require('../../weights/num2/model.json');
    // const modelWeights2 = require('../../weights/num2/group1-shard1of1.bin');
    //
    // const model2 =
    //     await tf.loadGraphModel(bundleResourceIO(modelJson2, modelWeights2));
    // const model3 =  async () => {
    //       const res = model.predict(tf.randomNormal([1, 10])) as tf.Tensor;
    //       const data = await res.data();
    //       return JSON.stringify(data);
    //     };
    // console.log(model3);

     const model = await posenet.load({
      //architecture: "MobileNetV1",
      architecture: "ResNet50",
      outputStride: 16,
      inputResolution: { width: inputTensorWidth, height: inputTensorHeight },
      quantBytes: 2,
     });
    return model;

  }


  async handleImageTensorReady(
    images: IterableIterator<tf.Tensor3D>,
    updatePreview: () => void,
    gl: ExpoWebGLRenderingContext
  ) {
    const loop = async () => {
      const { modelName } = this.state;
      if (!AUTORENDER) {
        updatePreview();
      }

      if (modelName === "posenet") {
        if (this.state.posenetModel != null) {
          const imageTensor = images.next().value;
          const flipHorizontal = Platform.OS === "ios" ? false : true;
          const pose = await this.state.posenetModel.estimateSinglePose(
            imageTensor,
            {flipHorizontal}
          );
          this.setState({ pose });
          tf.dispose([imageTensor]);
        }
      } else {
        if (this.state.faceDetector != null) {
          const imageTensor = images.next().value;
          const returnTensors = false;
          const faces = await this.state.faceDetector.estimateFaces(
            imageTensor,
            returnTensors
          );

          this.setState({ faces });
          tf.dispose(imageTensor);
        }
      }

      if (!AUTORENDER) {
        gl.endFrameEXP();
      }
      this.rafID = requestAnimationFrame(loop);
    };

    loop();
  }

  componentWillUnmount() {
    if (this.rafID) {
      cancelAnimationFrame(this.rafID);
    }
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    const [ posenetModel] = await Promise.all([
      this.loadPosenetModel(),
    ]);

    this.setState({
      hasCameraPermission: status === "granted",
      isLoading: false,
      faceDetector: '',
      posenetModel,
    });
    let d = new Date();
    this.test_user = {
      id_test : this.props.testID,
      test    :this.props.testName,
      frames            : '',
      second_sensor     : '',
      operating_system  : Platform.OS,
      timestamp         : d.getTime()
    }
  }

  renderPose() {
    const MIN_KEYPOINT_SCORE = 0.7;
    const { pose } = this.state;
    if (pose != null) {
      const keypoints = pose.keypoints
        .filter((k) => k.score > MIN_KEYPOINT_SCORE)
        .map((k, i) => {
          return (
            <Circle
              key={`skeletonkp_${i}`}
              cx={k.position.x}
              cy={k.position.y}
              r="1"
              strokeWidth="0"
              fill="blue"
            />
          );
        });
      cara_x = '"Cara X": ' + pose.keypoints[0].position.x.toString();
      cara_y = '"Cara Y": ' + pose.keypoints[0].position.y.toString();
      hombro_izq_x = '"Hombro izq X": ' + pose.keypoints[5].position.x.toString();
      hombro_izq_y = '"Hombro izq Y": ' + pose.keypoints[5].position.y.toString();
      hombro_der_x = '"Hombro der X": ' + pose.keypoints[6].position.x.toString();
      hombro_der_y = '"Hombro der Y": ' + pose.keypoints[6].position.y.toString();
      codo_izq_x = '"Codo izq X": ' + pose.keypoints[7].position.x.toString();
      codo_izq_y = '"Codo izq Y": ' + pose.keypoints[7].position.y.toString();
      codo_der_x = '"Codo der X": ' + pose.keypoints[8].position.x.toString();
      codo_der_y = '"Codo der Y": ' + pose.keypoints[8].position.y.toString();
      muñeca_izq_x = '"Muñeca izq X": ' + pose.keypoints[9].position.x.toString();
      muñeca_izq_y = '"Muñeca izq Y": ' + pose.keypoints[9].position.y.toString();
      muñeca_der_x = '"Muñeca der X": ' + pose.keypoints[10].position.x.toString();
      muñeca_der_y = '"Muñeca der Y": ' + pose.keypoints[10].position.y.toString();
      cadera_izq_x = '"Cadera izq X": ' + pose.keypoints[11].position.x.toString();
      cadera_izq_y = '"Cader izq Y": ' + pose.keypoints[11].position.y.toString();
      cadera_der_x = '"Cadera der X": ' + pose.keypoints[12].position.x.toString();
      cadera_der_y = '"Cadera der Y": ' + pose.keypoints[12].position.y.toString();
      rodilla_izq_x = '"Rodilla izq X": ' + pose.keypoints[13].position.x.toString();
      rodilla_izq_y = '"Rodilla izq Y": ' + pose.keypoints[13].position.y.toString();
      rodilla_der_x = '"Rodilla der X": ' + pose.keypoints[14].position.x.toString();
      rodilla_der_y = '"Rodilla der Y": ' + pose.keypoints[14].position.y.toString();
      tobillo_izq_x = '"Tobillo izq X": ' + pose.keypoints[15].position.x.toString();
      tobillo_izq_y = '"Tobillo izq Y": ' + pose.keypoints[15].position.y.toString();
      tobillo_der_x = '"Tobillo der X": ' + pose.keypoints[16].position.x.toString();
      tobillo_der_y = '"Tobillo der Y": ' + pose.keypoints[16].position.y.toString();

      var parts_body = ["'leftShoulder_", "'rightShoulder_", "'leftElbow_", "'rightElbow_", "'leftWrist_", "'rightWrist_", "'leftHip_", "'rightHip_", "'leftKnee_", "'rightKnee_","'leftAnkle_"]
      var axes = ["x'", "y'"]
      if (this.state.screen.rec == true){
      if (GLOBAL.FRAMES.length != 0){
          GLOBAL.FRAMES = GLOBAL.FRAMES + ",'frame" +Date.now().toString() + "':{";
          }
      if (GLOBAL.FRAMES.length == 0){
          GLOBAL.FRAMES = GLOBAL.FRAMES + "'frame" +Date.now().toString() + "':{";
          }
      GLOBAL.FRAMES = GLOBAL.FRAMES + "'face_" + axes[0] + ":"
      GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[0].position.x.toString() + ",";
      GLOBAL.FRAMES = GLOBAL.FRAMES + "'face_" + axes[1] + ":"
      GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[0].position.y.toString() + ",";
       for (var i = 5; i < 16; i++){
         GLOBAL.FRAMES = GLOBAL.FRAMES + parts_body[i-5] + axes[0] + ":"
         GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[i].position.x.toString() + ",";
         GLOBAL.FRAMES = GLOBAL.FRAMES + parts_body[i-5] + axes[1] + ":"
         GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[i].position.y.toString() + ",";

       }
       GLOBAL.FRAMES = GLOBAL.FRAMES + "'rightAnkle_" + axes[0] + ":"
       GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[16].position.x.toString() + ",";
       GLOBAL.FRAMES = GLOBAL.FRAMES + "'rightAnkle_" + axes[1] + ":"
       GLOBAL.FRAMES = GLOBAL.FRAMES + pose.keypoints[16].position.y.toString();
       GLOBAL.FRAMES = GLOBAL.FRAMES + "}";
      }

      const adjacentKeypoints = posenet.getAdjacentKeyPoints(
        pose.keypoints,
        MIN_KEYPOINT_SCORE
      );

      let times            = new Date();
      var list_fps = [1]
      comparation = this.state.timer.text;

      if (this.fps > this.maxfps[this.timestamp_fps]){
        if (this.fps != "undefined"){
          this.maxfps[this.timestamp_fps] = this.fps;
        }
      }

      if(this.state.fps > this.avg_fps){
        var div = parseInt(this.avg_fps / this.avg_fps);
        for (var fps_i = 0; fps_i < this.avg_fps-1; fps_i++){
          list_fps.push(list_fps[fps_i] + div);
        }
      }

      else if(this.state.fps <= this.avg_fps){
        var div = parseInt(this.avg_fps / this.state.fps);
        for (var fps_i = 0; fps_i < this.state.fps-1; fps_i++){
          list_fps.push(list_fps[fps_i] + div);
        }
      }

      if (list_fps.includes(this.fps)){

              if (init_compar == comparation){
                init_compar = "OK"
              }

              if (comparation == "0" && init_compar == "OK") {
              this.test_user.frames = this.test_user.frames + '"frame' + counter_timestamp.toString() + '":{ "Timestamp": ' + times.getTime() + ", " + cara_x + ", " + cara_y + ", " + hombro_der_x + ", " + hombro_der_y + ", " + hombro_izq_x + ", " + hombro_izq_y + ", " + codo_der_x + ", " + codo_der_y + ", " + codo_izq_x + ", " + codo_izq_y + ", " + muñeca_der_x + ", " + muñeca_der_y + ", " + muñeca_izq_x + ", " + muñeca_izq_y + ", " + cadera_der_x  + ", " + cadera_der_y  + ", " + cadera_izq_x  + ", " + cadera_izq_y  + ", " + rodilla_der_x  + ", " + rodilla_der_y + ", " + rodilla_izq_x + ", " + rodilla_izq_y + ", " + tobillo_der_x + ", " + tobillo_der_y + ", " + tobillo_izq_x + ", " + tobillo_izq_y + "} ";
              init_compar = "NOT";
              }

              else if (comparation != "" && init_compar == "OK"){
              this.test_user.frames = this.test_user.frames + '"frame' + counter_timestamp.toString() + '":{ "Timestamp": ' + times.getTime() + ", " + cara_x + ", " + cara_y + ", " + hombro_der_x + ", " + hombro_der_y + ", " + hombro_izq_x + ", " + hombro_izq_y + ", " + codo_der_x + ", " + codo_der_y + ", " + codo_izq_x + ", " + codo_izq_y + ", " + muñeca_der_x + ", " + muñeca_der_y + ", " + muñeca_izq_x + ", " + muñeca_izq_y + ", " + cadera_der_x  + ", " + cadera_der_y  + ", " + cadera_izq_x  + ", " + cadera_izq_y  + ", " + rodilla_der_x  + ", " + rodilla_der_y + ", " + rodilla_izq_x + ", " + rodilla_izq_y + ", " + tobillo_der_x + ", " + tobillo_der_y + ", " + tobillo_izq_x + ", " + tobillo_izq_y + "}, ";
              counter_timestamp = counter_timestamp + 1;
              }
      }



      if (date_fps != Date()) {
        date_fps = Date();
        if (this.timestamp_fps >= this.maxfps.length){this.timestamp_fps = 0}
        this.maxfps[this.timestamp_fps] = this.fps;
        let sum = this.maxfps.reduce((previous, current) => current += previous);
        this.avg_fps = Math.round(sum / this.maxfps.length);
        if (this.avg_fps.toString() == "NaN"){this.avg_fps = 1}
        this.fps = 1;
        this.timestamp_fps++;
      }

      else if (date_fps == Date()) {
        this.fps++;
      }


      const skeleton = adjacentKeypoints.map(([from, to], i) => {
        return (
          <Line
            key={`skeletonls_${i}`}
            x1={from.position.x}
            y1={from.position.y}
            x2={to.position.x}
            y2={to.position.y}
            stroke="magenta"
            strokeWidth="1"
          />
        );
      });

      return (
        <Svg
          height="100%"
          width="100%"
          viewBox={`0 0 ${inputTensorWidth} ${inputTensorHeight}`}
        >
          {skeleton}
          {keypoints}
        </Svg>
      );
    } else {
      return null;
    }
  }

  renderFaces() {
    const { faces } = this.state;
    if (faces != null) {
      const faceBoxes = faces.map((f, fIndex) => {
        const topLeft = f.topLeft as number[];
        const bottomRight = f.bottomRight as number[];

        const landmarks = (f.landmarks as number[][]).map((l, lIndex) => {
          return (
            <Circle
              key={`landmark_${fIndex}_${lIndex}`}
              cx={l[0]}
              cy={l[1]}
              r="2"
              strokeWidth="0"
              fill="blue"
            />
          );
        });

        return (
          <G key={`facebox_${fIndex}`}>
            <Rect
              x={topLeft[0]}
              y={topLeft[1]}
              fill={"red"}
              fillOpacity={0.2}
              width={bottomRight[0] - topLeft[0]}
              height={bottomRight[1] - topLeft[1]}
            />
            {landmarks}
          </G>
        );
      });

      const flipHorizontal = Platform.OS === "ios" ? 1 : -1;
      return (
        <Svg
          height="100%"
          width="100%"
          viewBox={`0 0 ${inputTensorWidth} ${inputTensorHeight}`}
          scaleX={flipHorizontal}
        >
          {faceBoxes}
        </Svg>
      );
    } else {
      return null;
    }
  }

  render() {
    const { isLoading, modelName } = this.state;

    const Separator = () => <View style={styles.separator} />;

    // TODO File issue to be able get this from expo.
    // Caller will still need to account for orientation/phone rotation changes
    let textureDims: { width: number; height: number };
    if (Platform.OS === "ios") {
      textureDims = {
        height: 1920,
        width: 1080,
      };
    } else {
      textureDims = {
        height: 1200,
        width: 1600,
      };
    }

    const camView = (
      <View style={styles.cameraContainer}>
        <TensorCamera
          // Standard Camera props
          style={styles.camera}
          type={this.state.cameraType}
          zoom={0}
          // tensor related props
          cameraTextureHeight={textureDims.height}
          cameraTextureWidth={textureDims.width}
          resizeHeight={inputTensorHeight}
          resizeWidth={inputTensorWidth}
          resizeDepth={3}
          onReady={this.handleImageTensorReady}
          autorender={AUTORENDER}
        />
        <View style={styles.modelResults}>
          {modelName === "posenet" ? this.renderPose() : this.renderFaces()}
        </View>
        <View style={styles.containerButtons}>
                {
                  this.state.timer.state ?
                  <TouchableOpacity
                    style={ styles.counter }
                    onPress={ () => this.startTest() } disabled={true}>
                    <Text style={styles.counterText}>{ this.state.timer.text }</Text>
                  </TouchableOpacity> : null
                }
                {
                  this.state.screen.btnMain.show ?
                  <TouchableOpacity
                    style={ styles.button }
                    onPress={ () => this.btnMain( this.state ) }
                    disabled={ this.state.screen.btnMain.disabled }>
                    <Text style={styles.buttonText}>{ this.state.screen.btnMain.text }</Text>
                  </TouchableOpacity> : null
                }
                {
                  this.state.screen.btnBack.show ?
                  <TouchableOpacity
                  style={styles.button}
                  onPress={this.props.returnToMain}>
                      <Text style={styles.textButton}>Volver</Text>
                  </TouchableOpacity> : null
                }

              </View>
      </View>
    );

    return (
      <View style={{ width: "100%" }}>
        <Separator />

        {isLoading ? (
          <View style={[styles.loadingIndicator]}>
            <ActivityIndicator size="large" color="#FF0266" />
          </View>
        ) : (
          camView
        )}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  fixToText: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  loadingIndicator: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 200,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 1,
  },
    container: {
      flex: 1,
      justifyContent: "center",
      paddingHorizontal: 1,
      marginBottom: '5%'
    },
  cameraContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
  },
  camera: {
    position: "absolute",
    left: "11%",
    top: 1,
    width: 600 / 2,
    height: 800 / 2,
    zIndex: 1,
    borderWidth: 0,
    borderColor: "black",
    borderRadius: 0,
  },
  modelResults: {
    position: "absolute",
    left: "10%",
    top: 1,
    width: 600 / 2,
    height: 800 / 2,
    zIndex: 20,
    borderWidth: 0,
    borderColor: "black",
    borderRadius: 0,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  // containerButtons: {
  //   justifyContent: "center",
  //   alignSelf: "center",
  //   marginTop: '5%',
  //   //paddingHorizontal: 10,
  //   width: '100%',
  // },
    // camera: {
    //   flex: 1,
    //   alignItems: 'center',
    //   justifyContent: 'center',
    // },
    title: {
      fontSize: 24,
      justifyContent: "center",
      fontWeight: "bold",
      alignSelf: "center",
      marginTop: 20,
      marginBottom: 30
    },
    button: {
      alignItems: "center",
      backgroundColor: "#00aaff",
      padding: "10%",
      margin: '2%',
      borderRadius: 10,
    },
    buttonText: {
      color: "white",
      fontSize: 18
    },
    counter: {
      alignItems: "center",
      padding: 10
    },
    textButton: {
      color: "#FFFFFF",
      fontSize: 18
    },
    counterText: {
      fontSize: 42
    },
      containerButtons: {
          position: "absolute",
          top: 450,
          width: 600 / 2,
          zIndex: 20
        //position: 'absolute',
        //bottom:'55%',
        //justifyContent: "center",
        //alignSelf: "center",
        //marginTop: '5%',
        //paddingHorizontal: 10,
        //width: '100%',
      }
});
