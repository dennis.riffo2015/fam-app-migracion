import React, {Fragment} from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity } from 'react-native';
import {
  Table,
  Row,
  Rows,
  Col,
} from 'react-native-table-component';

import { Diagnostic } from './diagnostic';
import Tts from 'react-native-tts';

import { getFrames, getFrames_plantilla } from '../database/Database';

export type Screen = 'main' | 'riskFactor' | 'riskFactorResult';


let frames_X:List = []
let frames_plantilla:List = []
var data_json
var data_json_plantilla

var puntaje_traslacion = 0
var puntaje_pendientes = 0

let riesgos:List = ["Improbable", "Poco probable", "Moderado", "Alto", "Crítico"]
let resultados:List = [0, 1, 2, 3, 4]

var resultado_general = "Alto"
var num_resultado_general = 5
var resultado_pendientes = "Malo"
var resultado_traslacion = "Malo"

let pendientes_cuerpo:List = ["Hombros", "Brazo superior Der", "Brazo superior izq", "Brazo inferior der", "Brazo inferior izq", "Torso der", "Torso izq", "Pelvis", "Pierna superior der", "Pierna superior izq", "Pierna inferior der", "Pierna inferior izq"]
let partes_cuerpo:List = ["Hombro der x","Hombro der y", "Codo der x", "Codo der y", "Codo izq x", "Codo izq y", "Muñeca der x", "Muñeca der y", "Muñeca izq x","Muñeca izq y", "Cadera der x", "Cadera der y", "Cadera izq x","Cadera izq y", "Rodilla der x", "Rodilla der y", "Rodilla izq x", "Rodilla izq y", "Tobillo der x", "Tobillo der y", "Tobillo izq x", "Tobillo izq y"]

let resultados_pendientes:List = [0,0,0,0,0,0,0,0,0,0,0,0]
let resultados_movimientos:List = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

var evaluacion_pendientes = "No existe"
var evaluacion_traslacion = "No existe"


interface ScreenProps {
    returnToMain: () => void;
    testChecked: Object[];
}
interface AppState {
    currentScreen: Screen;
    ready: Boolean;
}


export class RiskFactorResult extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'riskFactorResult',
          ready: false,
          backgroundColor: "#00aaff",
          text : 'Escuchar índice',
          pressed:false,
        }
        this.showMainScreen = this.showMainScreen.bind(this);
    };


    async runQuery(){
        frames_X = await getFrames(this.props.testChecked);
        //console.log(JSON.parse(frames_X[0]['id_test'].toString()))
        frames_plantilla = await getFrames_plantilla(JSON.parse(frames_X[0]['id_test'].toString()));
    }

    componentDidMount() {
        this.runQuery().then(() => this.setState({ready:true}));
        Tts.setDefaultRate(0.4);
        //console.log("FRAMES MOUNT "+ frames_X['_55']);
    }
    changeColor(){
    if(!this.state.pressed){
       this.setState({ pressed: true,backgroundColor: "#de3f5d" ,text:"Detener Instrucciones"});
    } else {
      this.setState({ pressed: false, backgroundColor: "#00aaff" ,text:"Escuchar índice"});
    }
  }
  async performTimeConsumingTask() {
    await sleep(4000);
  }

    updateButton(){
      if (!this.state.pressed){
        Tts.speak("Nivel de riesgo " + resultado_general.toString() + ", índice de riesgo" + num_resultado_general.toString(), {
        androidParams: {
          KEY_PARAM_PAN: -1,
          KEY_PARAM_VOLUME: 0.5,
          KEY_PARAM_STREAM: 'STREAM_MUSIC',
          },
          });
          // this.performTimeConsumingTask();
          //this.setState({ pressed: false, backgroundColor: "#00aaff" ,text:"Escuchar Instrucciones"});
      }
      else{
        Tts.stop();
      }
      Tts.addEventListener('tts-finish', (event) => this.setState({ pressed: false, backgroundColor: "#00aaff" ,text:"Escuchar índice"}));
    }

    distanceCalculation(list,keysframes_json, keys_json, i, a,b,c,d) {
      return Math.pow((Math.pow(list[keysframes_json[i]][keys_json[a]] - list[keysframes_json[i]][keys_json[b]],2) + Math.pow(list[keysframes_json[i]][keys_json[c]] - list[keysframes_json[i]][keys_json[d]],2)), 0.5)
    }
    distanceCalculation2(a,b,c,d) {
      return Math.pow((Math.pow(a - b,2) + Math.pow(c - d,2)), 0.5)
    }

    diferencia_vectores(p1x, p1y, p0x, p0y, factor){
      return [(p1x - p0x) * factor, (p1y - p0y) * factor]
    }

    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen} visible={false}/>
        </Fragment>;
    }
    renderRepButton(){
      return <Fragment>
      <TouchableOpacity
      style={{backgroundColor:this.state.backgroundColor, alignItems: "center",
      padding: '3%',
      margin: '3%',
    borderRadius: 10}}
      onPress={()=>{this.changeColor();this.updateButton();}}>
              <Text style={styles.textButton}>{this.state.text}</Text>
          </TouchableOpacity>
      </Fragment>

    }

    renderRiskFactorResult() {
        return <Fragment>
            <View>
              <Text style={styles.title}>Nivel de riesgo</Text>
             </View>
             <View style={{height: '3%'}}></View>
             <Text style={styles.textfactor}>A menor índice menor riesgo</Text>
             <Text style={styles.textfactor}>(El rango es de 0-5)</Text>
            <View style={styles.containerText}>

                    {/*<Text style={styles.textfactor}> {"Resultado Pendientes : " + evaluacion_pendientes}</Text>
                    <Text style={styles.textfactor}> {"Puntaje Pendientes : " + puntaje_pendientes.toString()}</Text>
                    <Text style={styles.textfactor}> {"Resultados Pendientes por separado: " + resultados_pendientes.toString()}</Text>
                    <Text style={styles.textfactor}> {"Resultado Traslación : " + evaluacion_traslacion.toString()}</Text>
                    <Text style={styles.textfactor}> {"Puntaje Traslación : " + puntaje_traslacion.toString()}</Text>*/}
                    <View style={{height: '3%'}}></View>
                    <Text style={styles.textfactorrisk}> {"Nivel de Riesgo"}</Text>
                    <View style={{height: '3%'}}></View>
                    <Text style={styles.textfactorrisk}> {resultado_general.toString()}</Text>
                    <View style={{height: '3%'}}></View>
                    <Text style={styles.textfactorrisk}> {"Índice de Riesgo" }</Text>
                    <View style={{height: '3%'}}></View>
                    <Text style={styles.textfactorrisk}> {num_resultado_general.toString()}</Text>
                    {/*<Text style={styles.textfactor}> {"Resultados Traslación por separado: " + resultados_movimientos.toString()}</Text>*/}
            </View>

            <View style={styles.container}>
                {this.renderRepButton()}
                <TouchableOpacity
                    style={styles.button}
                    onPress={this.props.returnToMain}
                >
                    <Text style={styles.textButton}>Volver</Text>
                </TouchableOpacity>
            </View>
      </Fragment >
    }

    renderContent() {
        const { currentScreen } = this.state;
        var works = 0;

        try {
          data_json = JSON.parse(frames_X[0]['frames'].toString());
          data_json_plantilla = JSON.parse(frames_plantilla[0]['demo_frames'].toString());
          var countframes_pl = Object.keys(data_json_plantilla).length;
          var count = Object.keys(data_json['frame0']).length;
          var keys_json = Object.keys(data_json['frame0']);
          var countframes = Object.keys(data_json).length;
          var keysframes_json = Object.keys(data_json);
          works = 1;
        } catch (e) {
          console.log(e);
        }
      if (works == 1){
      var menor_frames = 0
      if (countframes < countframes_pl){menor_frames = countframes}
      if (countframes >= countframes_pl){menor_frames = countframes_pl}

      for (var frames_test = 1; frames_test < countframes_pl; frames_test++){
        var framecount = 'frame' + frames_test;
        var framecountanterior = 'frame' + (frames_test - 1);
      //  console.log("Timestamp: " + data_json_plantilla[framecountanterior]['Timestamp'])
      //console.log("Resta entre frame " + (frames_test - 1) + " y " + (frames_test.toString()) + ": " + (data_json_plantilla[framecount]['Timestamp'] - data_json_plantilla[framecountanterior]['Timestamp']));
      }

      for (var frames_test = 1; frames_test < countframes; frames_test++){
        var framecount = 'frame' + frames_test;
        var framecountanterior = 'frame' + (frames_test - 1);
      //  console.log("Timestamp: " + data_json[framecountanterior]['Timestamp'])
      //console.log("Resta entre frame " + (frames_test - 1) + " y " + (frames_test.toString()) + ": " + (data_json[framecount]['Timestamp'] - data_json[framecountanterior]['Timestamp']));
      }


        //Lista de Promedios de pendientes y traslacion
        let promedios:List = [0,0,0,0,0,0,0,0,0,0,0,0]
        let promedios_movimientos:List = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        //Listas de Pendientes para test y test_plantilla
        let m:List = [0,0,0,0,0,0,0,0,0,0,0,0]
        let md:List = [0,0,0,0,0,0,0,0,0,0,0,0]

        //Lista de Distancia Plantilla y Test
        let dp:List = [0,0,0,0,0,0,0,0,0,0,0,0]
        let dt:List = [0,0,0,0,0,0,0,0,0,0,0,0]

        //Lista de Promedios traslacion Plantilla y Test
        let ptp:List = [0,0,0,0,0,0,0,0,0,0,0,0]
        let ptt:List = [0,0,0,0,0,0,0,0,0,0,0]


        //var puntos_traslado = data_json

        for (var l = 0; l < count; l++) {promedios_movimientos.push(0)}

        //var promedio_pendiente_hombro = 0


        for (let i = 0; i < menor_frames; i++) {

          //Calculo de Pendientes

          //Hombros
          m[0] = (data_json[keysframes_json[i]][keys_json[4]] - data_json[keysframes_json[i]][keys_json[6]]) / (data_json[keysframes_json[i]][keys_json[3]] - data_json[keysframes_json[i]][keys_json[5]])
          md[0] = (data_json_plantilla[keysframes_json[i]][keys_json[4]] - data_json_plantilla[keysframes_json[i]][keys_json[6]]) / (data_json_plantilla[keysframes_json[i]][keys_json[3]] - data_json_plantilla[keysframes_json[i]][keys_json[5]])
          //Brazo Superior Izq
          m[1] = (data_json[keysframes_json[i]][keys_json[8]] - data_json[keysframes_json[i]][keys_json[4]]) / (data_json[keysframes_json[i]][keys_json[7]] - data_json[keysframes_json[i]][keys_json[3]])
          md[1] = (data_json_plantilla[keysframes_json[i]][keys_json[8]] - data_json_plantilla[keysframes_json[i]][keys_json[4]]) / (data_json_plantilla[keysframes_json[i]][keys_json[7]] - data_json_plantilla[keysframes_json[i]][keys_json[3]])

          //Brazo Superior Der
          m[2] = (data_json[keysframes_json[i]][keys_json[10]] - data_json[keysframes_json[i]][keys_json[6]]) / (data_json[keysframes_json[i]][keys_json[9]] - data_json[keysframes_json[i]][keys_json[5]])
          md[2] = (data_json_plantilla[keysframes_json[i]][keys_json[10]] - data_json_plantilla[keysframes_json[i]][keys_json[6]]) / (data_json_plantilla[keysframes_json[i]][keys_json[9]] - data_json_plantilla[keysframes_json[i]][keys_json[5]])

          //Brazo Inferior Izq
          m[3] = (data_json[keysframes_json[i]][keys_json[12]] - data_json[keysframes_json[i]][keys_json[8]]) / (data_json[keysframes_json[i]][keys_json[11]] - data_json[keysframes_json[i]][keys_json[7]])
          md[3] = (data_json_plantilla[keysframes_json[i]][keys_json[12]] - data_json_plantilla[keysframes_json[i]][keys_json[8]]) / (data_json_plantilla[keysframes_json[i]][keys_json[11]] - data_json_plantilla[keysframes_json[i]][keys_json[7]])

          //Brazo Inferior Der
          m[4] = (data_json[keysframes_json[i]][keys_json[14]] - data_json[keysframes_json[i]][keys_json[10]]) / (data_json[keysframes_json[i]][keys_json[13]] - data_json[keysframes_json[i]][keys_json[9]])
          md[4] = (data_json_plantilla[keysframes_json[i]][keys_json[14]] - data_json_plantilla[keysframes_json[i]][keys_json[10]]) / (data_json_plantilla[keysframes_json[i]][keys_json[13]] - data_json_plantilla[keysframes_json[i]][keys_json[9]])

          //Torso Izq
          m[5] = (data_json[keysframes_json[i]][keys_json[16]] - data_json[keysframes_json[i]][keys_json[4]]) / (data_json[keysframes_json[i]][keys_json[15]] - data_json[keysframes_json[i]][keys_json[3]])
          md[5] = (data_json_plantilla[keysframes_json[i]][keys_json[16]] - data_json_plantilla[keysframes_json[i]][keys_json[4]]) / (data_json_plantilla[keysframes_json[i]][keys_json[15]] - data_json_plantilla[keysframes_json[i]][keys_json[3]])

          //Torso Der
          m[6] = (data_json[keysframes_json[i]][keys_json[18]] - data_json[keysframes_json[i]][keys_json[6]]) / (data_json[keysframes_json[i]][keys_json[17]] - data_json[keysframes_json[i]][keys_json[5]])
          md[6] = (data_json_plantilla[keysframes_json[i]][keys_json[18]] - data_json_plantilla[keysframes_json[i]][keys_json[6]]) / (data_json_plantilla[keysframes_json[i]][keys_json[17]] - data_json_plantilla[keysframes_json[i]][keys_json[5]])

          //Pelvis
          m[7] = (data_json[keysframes_json[i]][keys_json[16]] - data_json[keysframes_json[i]][keys_json[18]]) / (data_json[keysframes_json[i]][keys_json[15]] - data_json[keysframes_json[i]][keys_json[17]])
          md[7] = (data_json_plantilla[keysframes_json[i]][keys_json[16]] - data_json_plantilla[keysframes_json[i]][keys_json[18]]) / (data_json_plantilla[keysframes_json[i]][keys_json[15]] - data_json_plantilla[keysframes_json[i]][keys_json[17]])

          //Pierna Superior Izq
          m[8] = (data_json[keysframes_json[i]][keys_json[20]] - data_json[keysframes_json[i]][keys_json[16]]) / (data_json[keysframes_json[i]][keys_json[19]] - data_json[keysframes_json[i]][keys_json[15]])
          md[8] = (data_json_plantilla[keysframes_json[i]][keys_json[20]] - data_json_plantilla[keysframes_json[i]][keys_json[16]]) / (data_json_plantilla[keysframes_json[i]][keys_json[19]] - data_json_plantilla[keysframes_json[i]][keys_json[15]])

          //Pierna Superior Der
          m[9] = (data_json[keysframes_json[i]][keys_json[22]] - data_json[keysframes_json[i]][keys_json[18]]) / (data_json[keysframes_json[i]][keys_json[21]] - data_json[keysframes_json[i]][keys_json[17]])
          md[9] = (data_json_plantilla[keysframes_json[i]][keys_json[22]] - data_json_plantilla[keysframes_json[i]][keys_json[18]]) / (data_json_plantilla[keysframes_json[i]][keys_json[21]] - data_json_plantilla[keysframes_json[i]][keys_json[17]])

          //Pierna Inferior Izq
          m[10] = (data_json[keysframes_json[i]][keys_json[24]] - data_json[keysframes_json[i]][keys_json[20]]) / (data_json[keysframes_json[i]][keys_json[23]] - data_json[keysframes_json[i]][keys_json[19]])
          md[10] = (data_json_plantilla[keysframes_json[i]][keys_json[24]] - data_json_plantilla[keysframes_json[i]][keys_json[20]]) / (data_json_plantilla[keysframes_json[i]][keys_json[23]] - data_json_plantilla[keysframes_json[i]][keys_json[19]])

          //Pierna Inferior Der
          m[11] = (data_json[keysframes_json[i]][keys_json[26]] - data_json[keysframes_json[i]][keys_json[22]]) / (data_json[keysframes_json[i]][keys_json[25]] - data_json[keysframes_json[i]][keys_json[21]])
          md[11] = (data_json_plantilla[keysframes_json[i]][keys_json[26]] - data_json_plantilla[keysframes_json[i]][keys_json[22]]) / (data_json_plantilla[keysframes_json[i]][keys_json[25]] - data_json_plantilla[keysframes_json[i]][keys_json[21]])

          for (let j = 0; j < (m.length); j++){
            if (Math.abs(md[j]) > Math.abs(m[j])) {
              promedios[j] = promedios[j] + (Math.abs(md[j]) - Math.abs(m[j]))
            }
            if (Math.abs(m[j]) >= Math.abs(md[j])) {
              promedios[j] = promedios[j] + (Math.abs(m[j]) - Math.abs(md[j]))
            }
          }


          //Calculo de traslacion y recorrido de puntos

          //Distancia puntos de plantilla
          dp[0] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 3,5,4,6)
          dp[1] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 7,3,8,4)
          dp[2] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 9,5,10,6)
          dp[3] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 11,7,12,8)
          dp[4] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 13,9,14,10)
          dp[5] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 15,3,16,4)
          dp[6] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 17,5,18,6)
          dp[7] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 15,17,16,18)
          dp[8] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 19,15,20,16)
          dp[9] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 21,17,22,18)
          dp[10] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 23,19,24,20)
          dp[11] = this.distanceCalculation(data_json_plantilla, keysframes_json, keys_json, i, 25,21,26,22)

          //Distancia puntos de test
          dt[0] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 3,5,4,6)
          dt[1] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 7,3,8,4)
          dt[2] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 9,5,10,6)
          dt[3] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 11,7,12,8)
          dt[4] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 13,9,14,10)
          dt[5] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 15,3,16,4)
          dt[6] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 17,5,18,6)
          dt[7] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 15,17,16,18)
          dt[8] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 19,15,20,16)
          dt[9] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 21,17,22,18)
          dt[10] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 23,19,24,20)
          dt[11] = this.distanceCalculation(data_json, keysframes_json, keys_json, i, 25,21,26,22)


          var vector0 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[3]], data_json[keysframes_json[i]][keys_json[4]], data_json[keysframes_json[i]][keys_json[5]], data_json[keysframes_json[i]][keys_json[6]],(dp[0]/dt[0]))
          var vector1 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[3]], data_json[keysframes_json[i]][keys_json[4]], data_json[keysframes_json[i]][keys_json[7]], data_json[keysframes_json[i]][keys_json[8]],(dp[1]/dt[1]))
          var vector2 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[5]], data_json[keysframes_json[i]][keys_json[6]], data_json[keysframes_json[i]][keys_json[9]], data_json[keysframes_json[i]][keys_json[10]],(dp[2]/dt[2]))
          var vector3 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[7]], data_json[keysframes_json[i]][keys_json[8]], data_json[keysframes_json[i]][keys_json[11]], data_json[keysframes_json[i]][keys_json[12]],(dp[3]/dt[3]))
          var vector4 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[9]], data_json[keysframes_json[i]][keys_json[10]], data_json[keysframes_json[i]][keys_json[13]], data_json[keysframes_json[i]][keys_json[14]],(dp[4]/dt[4]))
          var vector5 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[3]], data_json[keysframes_json[i]][keys_json[4]], data_json[keysframes_json[i]][keys_json[15]], data_json[keysframes_json[i]][keys_json[16]],(dp[5]/dt[5]))
          var vector6 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[5]], data_json[keysframes_json[i]][keys_json[6]], data_json[keysframes_json[i]][keys_json[17]], data_json[keysframes_json[i]][keys_json[18]],(dp[6]/dt[6]))
          var vector7 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[17]], data_json[keysframes_json[i]][keys_json[18]], data_json[keysframes_json[i]][keys_json[15]], data_json[keysframes_json[i]][keys_json[16]],(dp[7]/dt[7]))
          var vector8 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[15]], data_json[keysframes_json[i]][keys_json[16]], data_json[keysframes_json[i]][keys_json[19]], data_json[keysframes_json[i]][keys_json[20]],(dp[8]/dt[8]))
          var vector9 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[17]], data_json[keysframes_json[i]][keys_json[18]], data_json[keysframes_json[i]][keys_json[21]], data_json[keysframes_json[i]][keys_json[22]],(dp[9]/dt[9]))
          var vector10 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[19]], data_json[keysframes_json[i]][keys_json[20]], data_json[keysframes_json[i]][keys_json[23]], data_json[keysframes_json[i]][keys_json[24]],(dp[10]/dt[10]))
          var vector11 = this.diferencia_vectores(data_json[keysframes_json[i]][keys_json[21]], data_json[keysframes_json[i]][keys_json[22]], data_json[keysframes_json[i]][keys_json[25]], data_json[keysframes_json[i]][keys_json[26]],(dp[11]/dt[11]))


          var nuevo_punto_5_6 = [data_json_plantilla[keysframes_json[i]][keys_json[5]], data_json_plantilla[keysframes_json[i]][keys_json[6]]]
          var nuevo_punto_3_4 = [vector0[0] + nuevo_punto_5_6[0], vector0[1]  + nuevo_punto_5_6[1]]
          var nuevo_punto_7_8 = [vector1[0] + nuevo_punto_3_4[0], vector1[1]  + nuevo_punto_3_4[1]]
          var nuevo_punto_9_10 = [vector2[0] + nuevo_punto_5_6[0], vector2[1]  + nuevo_punto_5_6[1]]
          var nuevo_punto_11_12 = [vector3[0] + nuevo_punto_7_8[0], vector3[1]  + nuevo_punto_7_8[1]]
          var nuevo_punto_13_14 = [vector4[0] + nuevo_punto_9_10[0], vector4[1]  + nuevo_punto_9_10[1]]
          var nuevo_punto_15_16 = [vector5[0] + nuevo_punto_3_4[0], vector5[1]  + nuevo_punto_3_4[1]]
          var nuevo_punto_17_18 = [vector6[0] + nuevo_punto_5_6[0], vector6[1]  + nuevo_punto_5_6[1]]
          var nuevo_punto_19_20 = [vector8[0] + nuevo_punto_15_16[0], vector8[1]  + nuevo_punto_15_16[1]]
          var nuevo_punto_21_22 = [vector9[0] + nuevo_punto_17_18[0], vector9[1]  + nuevo_punto_17_18[1]]
          var nuevo_punto_23_24 = [vector10[0] + nuevo_punto_19_20[0], vector10[1]  + nuevo_punto_19_20[1]]
          var nuevo_punto_25_26 = [vector11[0] + nuevo_punto_21_22[0], vector11[1]  + nuevo_punto_21_22[1]]


          if (0 < i && i < countframes){

            for (let j = 0; j < (ptp.length); j++){
              ptp[j]=[data_json_plantilla[keysframes_json[i-1]][keys_json[((j+1)*2)+1]],data_json_plantilla[keysframes_json[i-1]][keys_json[((j+1)*2)+2]]]
            }

            promedios_movimientos[0] = promedios_movimientos[0] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[3]] - ptp[0][0]) - (nuevo_punto_3_4[0] - ptt[0][0]))
            promedios_movimientos[1] = promedios_movimientos[1] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[4]] - ptp[0][1]) - (nuevo_punto_3_4[1] - ptt[0][1]))
            promedios_movimientos[2] = promedios_movimientos[2] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[7]] - ptp[2][0]) - (nuevo_punto_7_8[0] - ptt[1][0]))
            promedios_movimientos[3] = promedios_movimientos[3] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[8]] - ptp[2][1]) - (nuevo_punto_7_8[1] - ptt[1][1]))
            promedios_movimientos[4] = promedios_movimientos[4] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[9]] - ptp[3][0]) - (nuevo_punto_9_10[0] - ptt[2][0] ))
            promedios_movimientos[5] = promedios_movimientos[5] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[10]] - ptp[3][1]) - (nuevo_punto_9_10[1] - ptt[2][1] ))
            promedios_movimientos[6] = promedios_movimientos[6] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[11]] - ptp[4][0]) - (nuevo_punto_11_12[0] - ptt[3][0]))
            promedios_movimientos[7] = promedios_movimientos[7] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[12]] - ptp[4][1]) - (nuevo_punto_11_12[1] - ptt[3][1]))
            promedios_movimientos[8] = promedios_movimientos[8] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[13]] - ptp[5][0]) - (nuevo_punto_13_14[0] - ptt[4][0]))
            promedios_movimientos[9] = promedios_movimientos[9] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[14]] - ptp[5][1]) - (nuevo_punto_13_14[1] - ptt[4][1]))
            promedios_movimientos[10] = promedios_movimientos[10] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[15]] - ptp[6][0]) - (nuevo_punto_15_16[0] - ptt[5][0]))
            promedios_movimientos[11] = promedios_movimientos[11] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[16]] - ptp[6][1]) - (nuevo_punto_15_16[1] - ptt[5][1]))
            promedios_movimientos[12] = promedios_movimientos[12] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[17]] - ptp[7][0]) - (nuevo_punto_17_18[0] - ptt[6][0]))
            promedios_movimientos[13] = promedios_movimientos[13] + Math.abs(( data_json_plantilla[keysframes_json[i]][keys_json[18]] - ptp[7][1]) - (nuevo_punto_17_18[1] - ptt[6][1]))
            promedios_movimientos[14] = promedios_movimientos[14] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[19]] - ptp[8][0]) - (nuevo_punto_19_20[0] - ptt[7][0]))
            promedios_movimientos[15] = promedios_movimientos[15] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[20]] - ptp[8][1]) - (nuevo_punto_19_20[1] - ptt[7][1]))
            promedios_movimientos[16] = promedios_movimientos[16] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[21]] - ptp[9][0]) - (nuevo_punto_21_22[0] - ptt[8][0]))
            promedios_movimientos[17] = promedios_movimientos[17] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[22]] - ptp[9][1]) - (nuevo_punto_21_22[1] - ptt[8][1]))
            promedios_movimientos[18] = promedios_movimientos[18] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[23]] - ptp[10][0]) - (nuevo_punto_23_24[0] - ptt[9][0]))
            promedios_movimientos[19] = promedios_movimientos[19] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[24]] - ptp[10][1]) - (nuevo_punto_23_24[1] - ptt[9][1]))
            promedios_movimientos[20] = promedios_movimientos[20] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[25]] - ptp[11][0]) - (nuevo_punto_25_26[0] - ptt[10][0]))
            promedios_movimientos[21] = promedios_movimientos[21] + Math.abs((data_json_plantilla[keysframes_json[i]][keys_json[26]] - ptp[11][1]) - (nuevo_punto_25_26[1] - ptt[10][1]))

            // promedios_movimientos[0] = promedios_movimientos[0] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[3]],ptp[0][0],data_json_plantilla[keysframes_json[i]][keys_json[4]],ptp[0][1]) - this.distanceCalculation2(nuevo_punto_3_4[0],ptt[0][0],nuevo_punto_3_4[1],ptp[0][1]))
            // promedios_movimientos[1] = promedios_movimientos[1] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[3]],ptp[0][0],data_json_plantilla[keysframes_json[i]][keys_json[4]],ptp[0][1]) - this.distanceCalculation2(nuevo_punto_3_4[0],ptt[0][0],nuevo_punto_3_4[1],ptp[0][1]))
            // promedios_movimientos[2] = promedios_movimientos[2] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[7]],ptp[1][0],data_json_plantilla[keysframes_json[i]][keys_json[8]],ptp[1][1]) - this.distanceCalculation2(nuevo_punto_7_8[0],ptt[1][0],nuevo_punto_7_8[1],ptp[1][1]))
            // promedios_movimientos[3] = promedios_movimientos[3] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[7]],ptp[1][0],data_json_plantilla[keysframes_json[i]][keys_json[8]],ptp[1][1]) - this.distanceCalculation2(nuevo_punto_7_8[0],ptt[1][0],nuevo_punto_7_8[1],ptp[1][1]))
            // promedios_movimientos[4] = promedios_movimientos[4] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[9]],ptp[2][0],data_json_plantilla[keysframes_json[i]][keys_json[10]],ptp[2][1]) - this.distanceCalculation2(nuevo_punto_9_10[0],ptt[2][0],nuevo_punto_9_10[1],ptp[2][1]))
            // promedios_movimientos[5] = promedios_movimientos[5] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[9]],ptp[2][0],data_json_plantilla[keysframes_json[i]][keys_json[10]],ptp[2][1]) - this.distanceCalculation2(nuevo_punto_9_10[0],ptt[2][0],nuevo_punto_9_10[1],ptp[2][1]))
            // promedios_movimientos[6] = promedios_movimientos[6] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[11]],ptp[3][0],data_json_plantilla[keysframes_json[i]][keys_json[12]],ptp[3][1]) - this.distanceCalculation2(nuevo_punto_11_12[0],ptt[3][0],nuevo_punto_11_12[1],ptp[3][1]))
            // promedios_movimientos[7] = promedios_movimientos[7] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[11]],ptp[3][0],data_json_plantilla[keysframes_json[i]][keys_json[12]],ptp[3][1]) - this.distanceCalculation2(nuevo_punto_11_12[0],ptt[3][0],nuevo_punto_11_12[1],ptp[3][1]))
            // promedios_movimientos[8] = promedios_movimientos[8] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[13]],ptp[4][0],data_json_plantilla[keysframes_json[i]][keys_json[14]],ptp[4][1]) - this.distanceCalculation2(nuevo_punto_13_14[0],ptt[4][0],nuevo_punto_13_14[1],ptp[4][1]))
            // promedios_movimientos[9] = promedios_movimientos[9] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[13]],ptp[4][0],data_json_plantilla[keysframes_json[i]][keys_json[14]],ptp[4][1]) - this.distanceCalculation2(nuevo_punto_13_14[0],ptt[4][0],nuevo_punto_13_14[1],ptp[4][1]))
            // promedios_movimientos[10] = promedios_movimientos[10] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[15]],ptp[5][0],data_json_plantilla[keysframes_json[i]][keys_json[16]],ptp[5][1]) - this.distanceCalculation2(nuevo_punto_15_16[0],ptt[5][0],nuevo_punto_15_16[1],ptp[5][1]))
            // promedios_movimientos[11] = promedios_movimientos[11] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[15]],ptp[5][0],data_json_plantilla[keysframes_json[i]][keys_json[16]],ptp[5][1]) - this.distanceCalculation2(nuevo_punto_15_16[0],ptt[5][0],nuevo_punto_15_16[1],ptp[5][1]))
            // promedios_movimientos[12] = promedios_movimientos[12] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[17]],ptp[6][0],data_json_plantilla[keysframes_json[i]][keys_json[18]],ptp[6][1]) - this.distanceCalculation2(nuevo_punto_17_18[0],ptt[6][0],nuevo_punto_17_18[1],ptp[6][1]))
            // promedios_movimientos[13] = promedios_movimientos[13] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[17]],ptp[6][0],data_json_plantilla[keysframes_json[i]][keys_json[18]],ptp[6][1]) - this.distanceCalculation2(nuevo_punto_17_18[0],ptt[6][0],nuevo_punto_17_18[1],ptp[6][1]))
            // promedios_movimientos[14] = promedios_movimientos[14] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[19]],ptp[7][0],data_json_plantilla[keysframes_json[i]][keys_json[20]],ptp[7][1]) - this.distanceCalculation2(nuevo_punto_19_20[0],ptt[7][0],nuevo_punto_19_20[1],ptp[7][1]))
            // promedios_movimientos[15] = promedios_movimientos[15] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[19]],ptp[7][0],data_json_plantilla[keysframes_json[i]][keys_json[20]],ptp[7][1]) - this.distanceCalculation2(nuevo_punto_19_20[0],ptt[7][0],nuevo_punto_19_20[1],ptp[7][1]))
            // promedios_movimientos[16] = promedios_movimientos[16] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[21]],ptp[8][0],data_json_plantilla[keysframes_json[i]][keys_json[22]],ptp[8][1]) - this.distanceCalculation2(nuevo_punto_21_22[0],ptt[8][0],nuevo_punto_21_22[1],ptp[8][1]))
            // promedios_movimientos[17] = promedios_movimientos[17] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[21]],ptp[8][0],data_json_plantilla[keysframes_json[i]][keys_json[22]],ptp[8][1]) - this.distanceCalculation2(nuevo_punto_21_22[0],ptt[8][0],nuevo_punto_21_22[1],ptp[8][1]))
            // promedios_movimientos[18] = promedios_movimientos[18] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[23]],ptp[9][0],data_json_plantilla[keysframes_json[i]][keys_json[24]],ptp[9][1]) - this.distanceCalculation2(nuevo_punto_23_24[0],ptt[9][0],nuevo_punto_23_24[1],ptp[9][1]))
            // promedios_movimientos[19] = promedios_movimientos[19] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[23]],ptp[9][0],data_json_plantilla[keysframes_json[i]][keys_json[24]],ptp[9][1]) - this.distanceCalculation2(nuevo_punto_23_24[0],ptt[9][0],nuevo_punto_23_24[1],ptp[9][1]))
            // promedios_movimientos[20] = promedios_movimientos[20] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[25]],ptp[10][0],data_json_plantilla[keysframes_json[i]][keys_json[26]],ptp[10][1]) - this.distanceCalculation2(nuevo_punto_25_26[0],ptt[10][0],nuevo_punto_25_26[1],ptp[10][1]))
            // promedios_movimientos[21] = promedios_movimientos[21] + Math.abs(this.distanceCalculation2(data_json_plantilla[keysframes_json[i]][keys_json[25]],ptp[10][0],data_json_plantilla[keysframes_json[i]][keys_json[26]],ptp[10][1]) - this.distanceCalculation2(nuevo_punto_25_26[0],ptt[10][0],nuevo_punto_25_26[1],ptp[10][1]))

          }

        ptt[0] = nuevo_punto_3_4
        ptt[1] = nuevo_punto_7_8
        ptt[2] = nuevo_punto_9_10
        ptt[3] = nuevo_punto_11_12
        ptt[4] = nuevo_punto_13_14
        ptt[5] = nuevo_punto_15_16
        ptt[6] = nuevo_punto_17_18
        ptt[7] = nuevo_punto_19_20
        ptt[8] = nuevo_punto_21_22
        ptt[9] = nuevo_punto_23_24
        ptt[10] = nuevo_punto_25_26
      }

      for (let z = 0; z < 22; z++){
        resultados_movimientos[z] = promedios_movimientos[z] / countframes
        resultados_movimientos[z] = "\n" + partes_cuerpo[z] + " : " + resultados_movimientos[z]
      }

      for (let z = 0; z < 12; z++){
        resultados_pendientes[z] = promedios[z] / countframes
        resultados_pendientes[z] = "\n" + pendientes_cuerpo[z] + " : " + resultados_pendientes[z]
      }

        //Resultados Pendientes
        for (let j = 0; j < (m.length); j++){
          promedios[j] = promedios[j] / countframes
          puntaje_pendientes = puntaje_pendientes + promedios[j]
        }

        //Resultados Traslacion

        for (let k = 0; k < promedios_movimientos.length; k++){
          //console.log("k"+ k + " : " + promedios_movimientos[k])
          promedios_movimientos[k] = promedios_movimientos[k] / countframes
          puntaje_traslacion = puntaje_traslacion + promedios_movimientos[k]

        }

        //Promedios Totales
        puntaje_pendientes = puntaje_pendientes / m.length
        puntaje_traslacion = puntaje_traslacion / promedios_movimientos.length


        //Evaluacion de puntajes
        if (puntaje_pendientes < 5){evaluacion_pendientes = resultados[0]}
        else if (5 <= puntaje_pendientes && puntaje_pendientes < 10){evaluacion_pendientes = resultados[1]}
        else if (10 <= puntaje_pendientes && puntaje_pendientes < 15){evaluacion_pendientes = resultados[2]}
        else if (15 <= puntaje_pendientes && puntaje_pendientes < 25){evaluacion_pendientes = resultados[3]}
        else if (25 < puntaje_pendientes){evaluacion_pendientes = resultados[4]}

        if (parseFloat(puntaje_traslacion) < 0.7){evaluacion_traslacion = resultados[0]}
        else if (0.7 <= puntaje_traslacion && puntaje_traslacion < 1.4){evaluacion_traslacion = resultados[1]}
        else if (1.4 <= puntaje_traslacion && puntaje_traslacion < 1.9){evaluacion_traslacion = resultados[2]}
        else if (1.9 <= puntaje_traslacion && puntaje_traslacion < 2.5){evaluacion_traslacion = resultados[3]}
        else if (2.5 <= puntaje_traslacion){evaluacion_traslacion = resultados[4]}

        resultado_general = riesgos[Math.trunc((evaluacion_pendientes + evaluacion_traslacion)/2)]
        num_resultado_general = (evaluacion_pendientes + evaluacion_traslacion)/2

      }
      else {
        evaluación_pendientes = "No existe comparativa";
        evaluación_traslacion = "No existe comparativa";
      }

              switch (currentScreen) {
              case 'main':
                  return this.renderMainScreen();
              default:
                  return this.renderRiskFactorResult();
              }
    }

    render() {
        return (
        (this.state.ready) ? (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        ) : (
            <Fragment></Fragment>
        )
      );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white'
    },
    containerText: {
        marginVertical: "5%",
        justifyContent: "center",
        paddingHorizontal: 10,
        color: 'black',
        height: '50%',
      },
    container: {
      justifyContent: "center",
      paddingHorizontal: 10,
    },
    title: {
        fontSize: 24,
        justifyContent: "center",
        fontWeight: "bold",
        alignSelf: "center",
        marginTop: 20,
        marginBottom: 30,
        color: 'black'
      },
    button: {
        alignItems: "center",
        backgroundColor: "#00aaff",
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    textfactor: {
      textAlign: 'center',
    alignSelf: "center",
        color: 'black',
        fontSize: 23
    },
    textfactorrisk: {
      textAlign: 'center',
      alignSelf: "center",
        color: '#01558c',
        fontSize: 30
    },
    textButton: {
        color: "#FFFFFF",
        fontSize: 22
    },
    countContainer: {
      alignItems: "center",
      padding: 10
    }
  });
