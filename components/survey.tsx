import React, { Fragment} from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity, Picker, Dimensions  } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { Diagnostic } from '../components/diagnostic';
import { getCuestionario} from '../database/Database';

export type Screen = 'main' | 'survey';
let cuestionario:List = [];

interface ScreenProps {
    returnToMain: () => void;
}
interface AppState {
    currentScreen: Screen;
    screenHeight: Text;
}

export class Survey extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'survey',
          screenHeight: Dimensions.get('window').height,
          selected: []
        }
        this.showMainScreen = this.showMainScreen.bind(this);
    };

    async runQuery(){
        cuestionario = await getCuestionario();
        cuestionario.map( preg => this.state.selected.push({label: 'Item 1', value: preg.id_pregunta}))
    }

    componentDidMount() {
        this.runQuery().then(() => this.setState({ready:true}));
    }

    showMainScreen() {
        //cuestionario = getCuestionario();

        this.setState({ currentScreen: 'main' });
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen}/>
        </Fragment>;
    }

    onValueChange(value: string) {
    this.setState({
      selected: value,
    });
  }
  onValueChange2(value: string) {
  //for (var l = 0; l < this.state.selected.length; l++) {
    //console.log(this.state.selected[l])
    console.log(value)
  //}
}

    renderdropdown(id_pregunta,pregunta, respuesta){
      var data_json = JSON.parse(respuesta)
      var frames = Object.keys(data_json)
      console.log(frames)
      let data = []
      frames.map( preg => {
        data.push({value: data_json[preg]})
    })
    return (
      <View style={styles.container} key={id_pregunta}>
      <Text style={styles.subtitle}>{pregunta}</Text>
      <Dropdown key={id_pregunta}
        data={data}
      />
      </View>
    );
    }

    renderInput(id_pregunta,pregunta, respuesta) {
      if (respuesta == null){
        return <Fragment key={id_pregunta}>
        <Text style={styles.subtitle} >{pregunta} </Text>
        <TextInput style={styles.input}/>
        </Fragment>;
      }
      else {
        var data_json = JSON.parse(respuesta)
        var frames = Object.keys(data_json)
        console.log(frames)
        let data = []
        frames.map( preg => {
          data.push({value: data_json[preg]})
      })
      return <Fragment key={id_pregunta}>
        <Text style={styles.subtitle}>{pregunta}</Text>
        <Dropdown key={id_pregunta}
          data={data}
        />
        </Fragment>;
      }
    }

    renderSurvey() {
        //console.log(cuestionario);
        return <Fragment>
            <Text style={styles.title}>Cuestionario</Text>
            <Text style={styles.description}>Este cuestionario es opcional, las respuestas nos ayudarán a calcular con mayor precisión su factor de riesgo.</Text>
            <View style={styles.container}>
            <ScrollView style={{height: 400}}>
            {
                  cuestionario.map( preg =>
                    this.renderInput(preg.id_pregunta,preg.pregunta, preg.respuestas)
                )
          }

{/*cuestionario.map( preg => this.renderdropdown(preg.id_pregunta,preg.pregunta, preg.respuestas))*/}

            </ScrollView>
            <View>
            <TouchableOpacity
                style={styles.buttonSend}
                onPress={() => {}}
            >
                <Text  style={styles.textButton}>Enviar</Text>
            </TouchableOpacity>
            <TouchableOpacity
            style={styles.button}
            onPress={this.props.returnToMain}>
                <Text style={styles.textButton}>Volver</Text>
            </TouchableOpacity>
            </View>
            </View>
      </Fragment >
    }
    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        default:
            return this.renderSurvey();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
    },
    container: {
        paddingHorizontal: 10,
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    buttonSend: {
        alignItems: 'center',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    textButton: {
        color: "#FFFFFF",
        fontSize: 18
      },
    title: {
        fontSize: 24,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 30,
        color:'black'
    },
    subtitle: {
        fontSize: 20,
        margin: '1%',
        color:'black'
    },
    description: {
        marginHorizontal: 15,
        marginVertical: 10,
        padding: 10,
        backgroundColor: "#DDDDDD",
        textAlign: "justify",
        color:'black'
    },
    input: {
        borderColor: '#00aaff',
        borderWidth: 1,
        height: '7%'
    },
    picker: {
        borderColor: '#00aaff',
        borderWidth: 1,
        height: '7%',
    },
    text: {
        color: 'white',
        fontSize: 18,
    },
        dropdown: {
            backgroundColor: 'white',
            borderBottomColor: 'gray',
            borderBottomWidth: 0.5,
            marginTop: 20,
        },
        dropdown2: {
            backgroundColor: 'white',
            borderColor: 'gray',
            borderWidth: 0.5,
            marginTop: 20,
            padding: 8,
        },
        icon: {
            marginRight: 5,
            width: 18,
            height: 18,
        },
        item: {
            paddingVertical: 17,
            paddingHorizontal: 4,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        textItem: {
            flex: 1,
            fontSize: 16,
        },
});
