import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity } from 'react-native';

import { RealtimeDemo } from './webcam/realtime_demo';
import { RealtimePlantilla } from './webcam/realtime_demo_respaldo';
import { Demo } from './webcam/demo';
import Tts from 'react-native-tts';
import { Tests } from './tests';
GLOBAL = require('../components/global');

import { getOneTest } from '../database/Database';

export type Screen = 'main' | 'test' | 'tests' | 'realtime' | 'realtimeplantilla' | 'demo';


let frames_X:List = [];


interface ScreenProps {
    returnToMain: () => void;
    testChecked: Object[];
    name: String;
    instructions: String;
    duration: Number;
    url: String;
}
interface AppState {
    currentScreen: Screen;
}

interface ButtonState{

  screen:{
    state : number,
    rep   : boolean,
    btnMain : {
      disabled  : boolean,
      show : string,
      text : string
    }
 }
}

const test = [];

export class TestInstructions extends React.Component<ScreenProps, AppState, ButtonState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'test',
          id: 0,
          name: 'String',
          instructions: 'String',
          duration: 0,
          url: '',
          backgroundColor: "#00aaff",
          text : 'Escuchar Instrucciones',
          pressed:false,
        }
        this.showMainScreen = this.showMainScreen.bind(this);
        this.showTestsScreen = this.showTestsScreen.bind(this);
        this.showRealtimeDemo= this.showRealtimeDemo.bind(this);
        this.showRealtimePlantilla= this.showRealtimePlantilla.bind(this);
        this.showDemo= this.showDemo.bind(this);
    };

    async runQuery(){
        frames_X = await getOneTest(this.props.testChecked);
        this.setState({
          id: frames_X[0]['id_test'],
          name : frames_X[0]['name'],
          instructions:frames_X[0]['instructions'],
          duration: frames_X[0]['duration'],
          url: frames_X[0]['demo_url']
        })
        this.forceUpdate();
    }
    componentDidMount() {
        this.runQuery().then(() => this.setState({ready:true}));

    }

    changeColor(){
    if(!this.state.pressed){
       this.setState({ pressed: true,backgroundColor: "#de3f5d" ,text:"Detener Instrucciones"});
    } else {
      this.setState({ pressed: false, backgroundColor: "#00aaff" ,text:"Escuchar Instrucciones"});
    }
  }

    updateButton(){
      if (!this.state.pressed){
        Tts.speak(this.state.instructions, {
        androidParams: {
          KEY_PARAM_PAN: -1,
          KEY_PARAM_VOLUME: 0.5,
          KEY_PARAM_STREAM: 'STREAM_MUSIC',
          },
          });
      }
      else{
        Tts.stop();
      }
      Tts.addEventListener('tts-finish', (event) => this.setState({ pressed: false, backgroundColor: "#00aaff" ,text:"Escuchar Instrucciones"}));
    }

    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }
    showTestsScreen = () => {
        this.setState({ currentScreen: 'tests' });
    }
    showDemo() {
        this.setState({ currentScreen: 'demo' });
    }
    showRealtimeDemo() {
        this.setState({ currentScreen: 'realtime' });
    }

    showRealtimePlantilla() {
        this.setState({ currentScreen: 'realtimeplantilla' });
    }

    renderTestsScreen() {
        return <Fragment>
          <Tests returnToMain={this.showMainScreen}/>
        </Fragment>;
    }
    renderRealtimeDemo() {
        return <Fragment>
          <RealtimeDemo returnToMain={this.showMainScreen} testID = {this.state.id} testName = {this.state.name} testDuration = {this.state.duration}/>
        </Fragment>;
    }
    renderDemo() {
        return <Fragment>
          <Demo returnToMain={this.showMainScreen} demoURL = {this.state.url}/>
        </Fragment>;
    }
    renderRealtimePlantilla() {
        return <Fragment>
          <RealtimePlantilla returnToMain={this.showMainScreen}/>
        </Fragment>;
    }

    renderRepButton(){
      return <Fragment>
      <TouchableOpacity
      style={{backgroundColor:this.state.backgroundColor, alignItems: "center",
      padding: '3%',
      margin: '3%',
    borderRadius: 10}}
      onPress={()=>{this.changeColor();this.updateButton();}}>
              <Text style={styles.buttonText}>{this.state.text}</Text>
          </TouchableOpacity>
      </Fragment>

    }

    renderTest() {
    return <Fragment>
        <View>
          <Text style={styles.title}>Instrucciones</Text>
        </View>
        <View style={styles.instructions}>
          <ScrollView style={{height: "47%"}}>
                <Text style={styles.instructionsTitle}>{GLOBAL.TEST_NAME = this.state.name}</Text>
                <Text style={styles.instructionsText}>{this.state.instructions}</Text>
                <Text style={styles.instructionsText}>{"Duración: "+ this.state.duration + " segundos"}</Text>
                </ScrollView>
                </View>
        <View style={styles.container}>
          {this.renderRepButton()}


            <TouchableOpacity
                style={styles.button}
                onPress={this.showDemo}
            >
                <Text style={styles.buttonText}>Demostración</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.button}
                onPress={this.showRealtimeDemo}
            >
                <Text style={styles.buttonText}>Iniciar</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.button}
                onPress={this.props.returnToMain}
            >
                <Text style={styles.buttonText}>Volver</Text>
            </TouchableOpacity>
        </View>
      </Fragment >
    }

    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'tests':
            return this.renderTestsScreen();
        case 'demo':
            return this.renderDemo();
        case 'realtime':
            return this.renderRealtimeDemo();
        case 'realtimeplantilla':
            return this.renderRealtimePlantilla();
        default:
            return this.renderTest();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        paddingHorizontal: 10,
      },
      title: {
          fontSize: 24,
          justifyContent: "center",
          fontWeight: "bold",
          alignSelf: "center",
          marginTop: 20,
          marginBottom: 30,
          color: 'black'
      },
      subtitle: {
          fontSize: 20,
          justifyContent: "flex-start",
          marginLeft: 20,
          color: 'black'
      },
      instructions: {
          marginHorizontal: 15,
          marginVertical: 10,
          padding: 10,
          backgroundColor: "#DDDDDD",
          textAlign: "justify",
          color: 'black'
      },
      instructionsTitle: {
          fontSize: 22,
          padding: 10,
          textAlign: "justify",
          fontWeight: "bold",
          color: 'black'
      },
      instructionsText: {
          fontSize: 20,
          padding: 10,
          textAlign: "justify",
          color: 'black'
      },
      button: {
          alignItems: "center",
          backgroundColor: "#00aaff",
          padding: '3%',
          margin: '3%',
          borderRadius: 10,
      },
      buttonrep: {
          alignItems: "center",
          padding: '3%',
          margin: '3%',
          borderRadius: 10,
      },
      buttonText: {
          color: "white",
          fontSize: 18
      },
      countContainer: {
        alignItems: "center",
        padding: 10
      }
});
