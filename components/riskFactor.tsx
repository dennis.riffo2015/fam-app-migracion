import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity } from 'react-native';
import CheckBox from 'react-native-check-box';

import { getTestsUser, getNames_test } from '../database/Database';
import { Diagnostic } from './diagnostic';
import { RiskFactorResult } from './riskFactorResult';

export type Screen = 'main' | 'riskFactor' | 'riskFactorResult';

let tests_names:List = [];

interface ScreenProps {
    returnToMain: () => void;
}
interface AppState {
    currentScreen: Screen;
    loading: boolean;
    lista: TestUser[] | [];
    checked: Object[];
    list_view: string[];
    list_check: string[];
  }
interface TestUser {
    id_test_user : number;
    id_test: number;
    frames: string;
    second_sensor: string;
    timestamp: number;
    timestamp_date: string;
    timestamp_time: string;
    sync: number;
    test: string;
    duration: number;
    checked: boolean;
  }

interface TestName {
  name : string;
}

export class RiskFactor extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'riskFactor',
          loading : true,
          lista : [],
          checked: [],
          backgroundColor: "#de3f5d",
          pressed: false,
          nameTest: [],
          list_check:[],
          list_view: ["Semi-Tandem","Tandem","Tiempo de contacto virtual","Manteniéndose en una pierna","Caminar de frente","Caminar hacia los lados","Cuatro esquinas","De transición"]

        }
        this.showMainScreen = this.showMainScreen.bind(this);
        this.showRiskFactorResult = this.showRiskFactorResult.bind(this);
    };
    componentDidMount(){
        getTestsUser().then((elements:TestUser[]) =>{
          elements.forEach(element => {
            let d = new Date(element.timestamp);
            element.timestamp_time = '';
            element.timestamp_date = '';
            if(element.timestamp != 1 && element.timestamp != 0 && element.timestamp != null){
              element.timestamp_time = (d.getMinutes()<10?'0':'')+d.getHours()+':'+(d.getMinutes()<10?'0':'')+d.getMinutes()+':'+(d.getSeconds()<10?'0':'')+d.getSeconds(),
              element.timestamp_date = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()
            }
            element.checked = false;
          });

          this.setState({
            lista : elements,
            loading:false,
            list_check: []
          })
          this.forceUpdate();
        })
        getNames_test().then((elements:TestName[]) =>{
        // console.log(elements);
        elements.forEach(element => {
        tests_names.push(element);})
      })
      // console.log(tests_names);
      }
    isItemChecked(id_test_user) {
      // this.state.list_check = [];
    if (this.state.checked.indexOf(id_test_user) > -1){
      //console.log(this.state.lista[id_test_user - 2]["test"]);
      this.state.list_check.push(this.state.lista[id_test_user - 2]["test"])
      //console.log(this.list_check);
    };
    return this.state.checked.indexOf(id_test_user) > -1
    }

    manageToggle = (evt, id_test_user) => {
      //console.log(id_test_user);

       this.changeColor();
    if (this.isItemChecked(id_test_user)) {
        this.setState({
        checked: this.state.checked.filter(i => i !== id_test_user)
      });

    } else {
        this.setState({
        checked: [...this.state.checked, id_test_user]
        })
    }
      // console.log(this.list_check);


    }

    changeColor(){
      console.log(this.state.list_check);
    if(!this.state.pressed && this.state.checked.length == 7){
       //console.log(this.state.checked);
       this.setState({ pressed: true,backgroundColor: "#00aaff"});
    } else {
      this.setState({ pressed: false, backgroundColor: "#de3f5d"});
    } if (this.state.checked.length != 7){
      this.setState({ pressed: false, backgroundColor: "#de3f5d"});
    }
    if (this.state.checked.length == 0){
      this.setState({ pressed: false, backgroundColor: "#de3f5d"});
    }
  }


    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }
    showRiskFactorResult() {
        this.setState({ currentScreen: 'riskFactorResult' });
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen} visible={false}/>
        </Fragment>;
    }
    renderRiskFactorResult() {
      //console.log(this.state.checked);
        return <Fragment>
          <RiskFactorResult returnToMain={this.showMainScreen} testChecked={this.state.checked}/>
        </Fragment>;
    }

    renderCalcButton(){
return <Fragment>
<TouchableOpacity disabled={!this.state.pressed}
    style={{alignItems: "center",
    backgroundColor: "#00aaff",
    padding: '3%',
    margin: '3%',
    backgroundColor:this.state.backgroundColor,
    borderRadius: 10
  }}
    onPress={this.showRiskFactorResult}
>
    <Text style={styles.textButton}>Calcular índice de riesgo</Text>
</TouchableOpacity>
</Fragment>
    }

    renderRiskFactor() {
        return <Fragment>
        <View>
            <Text style={styles.title}>Mis tests</Text>
        </View>
        <View>
            <Text style={styles.containerText}>Seleccione 8 tests diferentes para calcular el índice de riesgo.</Text>
        </View>
        <View style={styles.containerCheckbox}>
            {/* <CheckBox
                style={styles.checkbox}
                rightText={'Test 1 - 22-06-2021 - 17:00'}
                isChecked={this.state.checked}
                onClick={() => this.setState({checked: !this.state.checked})}
            /> */}
            <ScrollView style={{height: '43%'}}>
            {
              this.state.lista.map( test =>
                <CheckBox
                  style={styles.checkbox}
                  rightTextStyle={{color:'black'}}
                  tintColors={{ true: 'red' }, { false: 'green' }}
                  rightText={(test.id_test_user - 1) + ' - ' + test.test+ (test.timestamp_date != '' ? ' - '+test.timestamp_date : '')+ (test.timestamp_time != '' ? ' - '+test.timestamp_time : '')}
                  isChecked={this.isItemChecked(test.id_test_user)}
                  onClick={ evt => this.manageToggle(evt, test.id_test_user) }
                  onPress = {() => {this.changeColor()}}
                  key={test.id_test_user}

                />
              )
            }
            </ScrollView>
        </View>
        <View style={styles.containerButton}>
          {this.renderCalcButton()}
            <TouchableOpacity
                style={styles.button}
                onPress={this.props.returnToMain}
            >
                <Text style={styles.textButton}>Volver</Text>
            </TouchableOpacity>
        </View>
      </Fragment >
    }

    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        case 'riskFactorResult':
            return this.renderRiskFactorResult();
        default:
            return this.renderRiskFactor();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white'
    },
    containerText: {
      marginVertical: "5%",
      justifyContent: "center",
      paddingHorizontal: 10,
      color:'black',
      fontSize: 24,
      alignSelf: "center",
    },
    containerCheckbox: {
      marginVertical: "5%",
      justifyContent: "center",
      paddingHorizontal: 10,
    },
    containerButton: {
      marginVertical: "5%",
      justifyContent: "center",
      paddingHorizontal: 10,
    },
    title: {
      fontSize: 24,
      justifyContent: "center",
      fontWeight: "bold",
      alignSelf: "center",
      marginTop: 20,
      marginBottom: 30,
      color:'black'
    },
    button: {
      alignItems: "center",
      backgroundColor: "#00aaff",
      padding: '3%',
      margin: '3%',
      borderRadius: 10,
    },
    textButton: {
      color: "#FFFFFF",
      fontSize: 20
    },
    countContainer: {
      alignItems: "center",
      padding: 10
    },
    checkbox: {
      //backgroundColor: '#000000',
      padding: 10
      //color: 'black'

    }
  });
