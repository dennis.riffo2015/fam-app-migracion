import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity } from 'react-native';

import { TestInstructions } from '../components/testInstructions';
import { Diagnostic } from '../components/diagnostic';
import { RealtimePlantilla } from './webcam/realtime_demo_respaldo';
import { deleteTests, deletePlantillas} from '../database/Database';

export type Screen = 'main' | 'tests' | 'testInstructions' | 'realtimeplantilla' ;

interface ScreenProps {
    returnToMain: () => void;
  }
interface AppState {
    currentScreen: Screen;
}
export class Administrador extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'tests'
        }
        this.showMainScreen = this.showMainScreen.bind(this);
        //this.showTestInstructions= this.showTestInstructions.bind(this);
        this.delete_Tests = this.delete_Tests.bind(this);
        this.showRealtimePlantilla= this.showRealtimePlantilla.bind(this);
    };

    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }
    showTestsScreen() {
        this.setState({ currentScreen: 'tests' });
    }
    delete_Tests() {
        deleteTests();
    }
    delete_Plantillas() {
        deletePlantillas();
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen}/>
        </Fragment>;
    }
    showRealtimePlantilla() {
        this.setState({ currentScreen: 'realtimeplantilla' });
    }
    renderRealtimePlantilla() {
        return <Fragment>
          <RealtimePlantilla returnToMain={this.showMainScreen}/>
        </Fragment>;
    }
    renderTests() {
        return <Fragment>
        <Text style={styles.title}>Seleccione una acción</Text>
        <View>
        {/*

            <TouchableOpacity
            style={styles.button}
            onPress={this.delete_Plantillas}>
                <Text style={styles.text}>Borrar Plantillas</Text>
            </TouchableOpacity>
          */}
          <TouchableOpacity
          style={styles.button}
          onPress={this.showRealtimePlantilla}>
              <Text style={styles.text}>Crear Plantillas</Text>
          </TouchableOpacity>
            <TouchableOpacity
            style={styles.button}
            onPress={this.delete_Tests}>
                <Text style={styles.text}>Borrar Tests</Text>
            </TouchableOpacity>
            <View style={{height: '5%'}}></View>
            <TouchableOpacity
            style={styles.button}
            onPress={this.props.returnToMain}>
                <Text style={styles.text}>Volver</Text>
            </TouchableOpacity>
        </View>
      </Fragment >
    }
    renderTestInstructions() {
        return <Fragment>
          <TestInstructions returnToMain={this.showMainScreen}/>
        </Fragment>;
    }
    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        case 'realtimeplantilla':
            return this.renderRealtimePlantilla();
        default:
            return this.renderTests();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    title: {
        fontSize: 24,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 30,
        color: 'black'
    },
    text: {
        color: 'white',
        fontSize: 18,
    },
});
