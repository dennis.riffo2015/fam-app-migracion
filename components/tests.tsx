import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity, BackHandler } from 'react-native';
import ButtonGroup from '@ramonak/react-button-group';

import { TestInstructions } from '../components/testInstructions';
import { Diagnostic } from '../components/diagnostic';
import { getTests } from '../database/Database';

export type Screen = 'main' | 'tests' | 'testInstructions';

let frames_X:List = [];
var data_json:List = [];

interface ScreenProps {
    returnToMain: () => void;
  }
interface AppState {
    currentScreen: Screen;
    checked: Object[];
}

const buttons = [];

export class Tests extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'tests',
          selectedIndex: 1,
          checked: "Hola"
        }
        this.showMainScreen = this.showMainScreen.bind(this);
        this.showTestInstructions= this.showTestInstructions.bind(this);
    };


    async runQuery(){
        frames_X = await getTests();
          if (buttons.length == 0){
        for (let index = 0; index < frames_X.length; index++) {
            buttons.push({id: frames_X[index]['id_test'], text:frames_X[index]['name'],action: () => {console.log("checked")}});
        }
      }
    }

    componentDidMount() {
        this.runQuery().then(() => this.setState({ready:true}));
    }

    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }
    showTestsScreen() {
        this.setState({ currentScreen: 'tests' });
    }
    showTestInstructions(value) {
        this.setState({ currentScreen: 'testInstructions', checked: value});
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic returnToMain={this.showMainScreen}/>
        </Fragment>;
    }


    renderTests() {

      var buttons_data = Object.keys(buttons)
      var numcol = 0;
      const renderedButtons =  buttons.map(b => {
      numcol++;

if (b.id % 2) {

  try{
    //console.log(buttons[buttons_data[numcol]]['id']);
    return <View key={numcol-1} style={{ flexDirection: "row"}}>
            <View style={{ flex: 1 }}>
              <TouchableOpacity style={styles.button} onPress={() => {this.showTestInstructions(b.id)}} >
                <Text style={styles.text}>{b.text} </Text>
              </TouchableOpacity>
            </View>
            <View style={{borderLeftWidth: 1,borderLeftColor: 'white'}}/>
              <View style={{ flex: 1 }}>
                <TouchableOpacity  style={styles.buttonright} onPress={() => {this.showTestInstructions(b.id + 1)}} >
                  <Text style={styles.text}>{buttons[buttons_data[numcol]]['text']}
                  </Text>
                </TouchableOpacity>
              </View>
          </View>}
    catch{return <View key={numcol} style={{ flexDirection: "row"}}>
            <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.buttonright} onPress={() => {this.showTestInstructions(b.id)}} >
              <Text style={styles.text}>{b.text}
              </Text>
            </TouchableOpacity>
          </View>
          </View>
    }
}
  });
    return <Fragment>
    <View>
      <Text style={styles.title}>Seleccione un test</Text>
<View style={{height: '7%'}}></View>

    {
      renderedButtons

    }

    <View style={{height: '20%'}}></View>
    <View>
    <TouchableOpacity
        style={styles.button}
        onPress={this.props.returnToMain}
    >
        <Text style={styles.text}>Volver</Text>
    </TouchableOpacity>
    </View>
    </View>
    </Fragment>
    }
    renderTestInstructions() {
        return <Fragment>
          <TestInstructions returnToMain={this.showMainScreen} testChecked={this.state.checked}/>
        </Fragment>;
    }
    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        case 'testInstructions':
            return this.renderTestInstructions();
        default:
            return this.renderTests();
        }
    }

    render() {

      // const buttons = ['Camera', 'Gallery', 'Share'];
      // const { selectedIndex } = this.state;
      return (
        <Fragment>
        <View>
     {this.renderContent()}
     <View style={{height: '5%'}}></View>
    </View>
    </Fragment>

    )
        // return (
        //     <Fragment>
        //         <StatusBar barStyle='dark-content' />
        //         <SafeAreaView>
        //             <View style={styles.body}>
        //             {this.renderContent()}
        //             </View>
        //         </SafeAreaView>
        //     </Fragment>
        // );
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
    },
    button: {
        alignSelf: 'stretch',
        alignItems: 'stretch',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderRadius: 10,
    },
    buttonright: {
        alignSelf: 'stretch',
        alignItems: 'stretch',
        backgroundColor: '#00aaff',
        padding: '3%',
        margin: '3%',
        borderLeftWidth: 1,
        borderLeftColor: 'white',
        borderRadius: 10,
    },
    title: {
        fontSize: 28,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
        color: 'black'
    },
    text: {
      textAlign: 'center',
      alignSelf: "center",
        alignItems: "center",
        color: 'white',
        fontSize: 22,
    },
});
