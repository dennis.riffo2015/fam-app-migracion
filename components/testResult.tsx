import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity } from 'react-native';

import { Diagnostic } from './diagnostic';
import { TestInstructions } from './testInstructions';

export type Screen = 'main' | 'testInstructions' | 'testResult';

interface ScreenProps {
    returnToMain: () => void;
}
interface AppState {
    currentScreen: Screen;
    checked: boolean;
}

export class TestResult extends React.Component<ScreenProps, AppState> {
    constructor(props: ScreenProps) {
        super(props);
        this.state = {
          currentScreen: 'testResult',
          checked: false
        }
        this.showMainScreen = this.showMainScreen.bind(this);
        this.showTestInstructions = this.showTestInstructions.bind(this);
    };

    showMainScreen() {
        this.setState({ currentScreen: 'main' });
    }
    showTestInstructions() {
        this.setState({ currentScreen: 'testInstructions' });
    }

    renderMainScreen() {
        return <Fragment>
          <Diagnostic visible={false}/>
        </Fragment>;
    }
    renderTestInstructions() {
        return <Fragment>
          <TestInstructions returnToMain={this.showMainScreen}/>
        </Fragment>;
    }

    renderTestResult() {
        return <Fragment>
        <View style={styles.results}>
            <Text>RESULTADOS DE TEST REALIZADO</Text>
            <Text>TEST : Test 1</Text>
            <Text>Fecha : 22-06-2021</Text>
            <Text>Hora : 17:00</Text>
        </View>
        
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.button}
                onPress={this.showTestInstructions}
            >
                <Text  style={styles.buttonText}>Reintentar</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.button}
                onPress={() => {} }
            >
                <Text  style={styles.buttonText}>Visualizar</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.button}
                onPress={this.showMainScreen}
            >
                <Text  style={styles.buttonText}>Menú principal</Text>
            </TouchableOpacity>
        </View>
      </Fragment >
    }

    renderContent() {
        const { currentScreen } = this.state;
        switch (currentScreen) {
        case 'main':
            return this.renderMainScreen();
        case 'testInstructions':
            return this.renderTestInstructions();
        default:
            return this.renderTestResult();
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle='dark-content' />
                <SafeAreaView>
                    <View style={styles.body}>
                    {this.renderContent()}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    body:{
        backgroundColor: 'white'
    },
    container: {
      justifyContent: "center",
      paddingHorizontal: 10,
    },
    results: {
      margin: '5%'
    },
    title: {
        fontSize: 24,
        justifyContent: "center",
        fontWeight: "bold",
        alignSelf: "center",
        marginTop: 20,
        marginBottom: 30
    },
    subtitle: {
        fontSize: 20,
        justifyContent: "flex-start",
        marginLeft: 20
    },
    instructions: {
        marginHorizontal: 15,
        marginVertical: 10,
        padding: 10,
        backgroundColor: "#DDDDDD",
        textAlign: "justify"
    },
    instructionsTitle: {
        fontSize: 20,
        padding: 10,
        textAlign: "justify",
        fontWeight: "bold"
    },
    instructionsText: {
        fontSize: 18,
        padding: 10,
        textAlign: "justify"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#00aaff",
        padding: 10,
        margin: 10
    },
    buttonText: {
        color: "white",
        fontSize: 18
    },
    countContainer: {
      alignItems: "center",
      padding: 10
    }
});