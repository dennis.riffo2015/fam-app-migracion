import React, { Fragment } from 'react';
import { Button, SafeAreaView, StyleSheet, View, Text, StatusBar, TextInput, Image, ScrollView, Modal, TouchableOpacity, BackHandler } from 'react-native';

import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-react-native';
import RNExitApp from 'react-native-exit-app';

import { MobilenetDemo } from '../components/mobilenet_demo';
import { TestRunner } from '../components/tfjs_unit_test_runner';
import { WebcamDemo } from '../components/webcam/webcam_demo';
import { RealtimeDemo } from '../components/webcam/realtime_demo';
import { Tests } from '../components/tests';
import { RiskFactor } from './riskFactor';
import { TestResult } from './testResult';
import { Survey } from './survey';
import { Educative } from './educative';
import { RealtimePlantilla } from './webcam/realtime_demo_respaldo';
import { Administrador } from '../components/administrador';
import { getImages } from '../database/Database';

const BACKEND_TO_USE = 'rn-webgl';
GLOBAL = require('global');

export type Screen = 'main' | 'tests' | 'demo' | 'webcam' | 'realtime' | 'riskFactor' | 'testResult' | 'survey' | 'realtimeplantilla' | 'admin' | 'educative';

interface ScreenProps {
  visible: boolean;
}
interface AppState {
  isTfReady: boolean;
  currentScreen: Screen;
  visible: boolean;
}

let frames_X:List = [];
var visiblemodal = true;

export class Diagnostic extends React.Component<ScreenProps, AppState> {
  //showModal = () => this.setState({ visible: true });
  hideModal = () => {this.setState({ visible: false });
    this.forceUpdate();
    console.log("Pulse");
    visiblemodal = false;
};
  constructor(props: ScreenProps) {
    super(props);
    this.state = {
      isTfReady: false,
      currentScreen: 'main',
      visible: visiblemodal,
      imageUri: ''
    };
    this.showMainScreen = this.showMainScreen.bind(this);
    this.showWebcamDemo= this.showWebcamDemo.bind(this);
    this.showRealtimeDemo= this.showRealtimeDemo.bind(this);
    this.showTests= this.showTests.bind(this);
    this.showAdmin= this.showAdmin.bind(this);
    this.showRiskFactor = this.showRiskFactor.bind(this);
    this.showTestResult = this.showTestResult.bind(this);
    this.showSurvey = this.showSurvey.bind(this);
    this.showEducative = this.showEducative.bind(this);
    this.showRealtimePlantilla= this.showRealtimePlantilla.bind(this);
  }

  async runQuery(){
      frames_X = await getImages();
      //console.log(frames_X[0]['image']);

      this.setState({
        imageUri: "data:image/png;base64," + frames_X[0]['image'],
      });
    }

  async componentDidMount() {

    const backAction = () => {
          this.showMainScreen();
          return true;
        };

    const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    await this.runQuery();
    await tf.setBackend(BACKEND_TO_USE);
    await tf.ready();

    this.setState({
      isTfReady: true,
      imageUri: "data:image/png;base64," + frames_X[0]['image'],
    });

  }

  // hideModal(){
  //   this.setState({ visible: false });
  //   this.forceUpdate();
  //   visiblemodal = false;
  //   console.log("Pulse")
  // }

  showMainScreen() {
    this.setState({ currentScreen: 'main' });
  }

  showRealtimeDemo() {
    this.setState({ currentScreen: 'realtime' });
  }

  showTests(){
    this.setState({ currentScreen: 'tests' });
  }

  showAdmin(){
    this.setState({ currentScreen: 'admin' });
  }

  showRiskFactor(){
    this.setState({ currentScreen: 'riskFactor' });
  }

  showTestResult(){
    this.setState({ currentScreen: 'testResult' });
  }
  showSurvey(){
    this.setState({ currentScreen: 'survey' });
  }

  showEducative(){
    this.setState({ currentScreen: 'educative' });
  }

  showWebcamDemo() {
     this.setState({ currentScreen: 'webcam' });
   }

  SpinningBox() {
    this.setState({ currentScreen: 'spinning' });
  }

  showRealtimePlantilla() {
      this.setState({ currentScreen: 'realtimeplantilla' });
  }

  exitapp(){
    BackHandler.exitApp()
  }
  

  renderMainScreen() {
    return <Fragment>
    <Modal visible={this.state.visible} onRequestClose={()=>{}} animationType="slide">
        <View style={styles.centeredView}>
        <View style={styles.modalView}>
            <Text style={styles.titleText}>Acuerdo de uso y exención de responsabilidad. {"\n"}</Text>
            <ScrollView contentContainerStyle={{paddingHorizontal: 0}} scrollEnabled={true}>
              <Text style={styles.textModal}>Nota: Esta aplicación está en desarrollo y está pensada en complementar la evaluación de un profesional de la salud y no la reemplaza en ningún momento.{"\n"}</Text>
              <Text style={styles.textModal}>Instrucciones de uso:</Text>
              <Text style={styles.textModal}></Text>
              <Text style={styles.textModal}>Busque un espacio que tenga el piso y pared lo más despejado posible (evitar alfombras, ventanas y muebles)</Text>
              <Text style={styles.textModal}>Enciende la luz (evita luz natural)</Text>
              <Text style={styles.textModal}>Debe posicionar su teléfono de forma vertical sobre una mesa.</Text>
              <Text style={styles.textModal}>Para los ejercicios debe ponerse de pie frente al teléfono para que la cámara frontal capte su cuerpo completo.</Text>
              <Text style={styles.textModal}></Text>
              <Text style={styles.textModal}>Precaución:</Text>
              <Text style={styles.textModal}></Text>
              <Text style={styles.textModal}>Para la evaluación de riesgo de caídas deberá realizar 8 ejercicios, si siente que se va a caer debe PARAR INMEDIATAMENTE ¡su salud es lo más importante! </Text>
              <Text style={styles.textModal}></Text>


            </ScrollView>
             <View>
            <TouchableOpacity
              style={styles.button}
              onPress={this.hideModal}>
              <Text style={styles.textButton}>Acepto</Text>
            </TouchableOpacity>
            </View>
        </View>
        </View>
    </Modal>
      <View style={styles.sectionContainer}>
          <Image
            source={{uri: this.state.imageUri, scale: 1}}
            style={{width: '90%', height: '40%', alignSelf:'center', margin:'3%' }}
          />
          <View>
        <TouchableOpacity
          style={styles.button}
          onPress={this.showTests}>
          <Text style={styles.textButton}>Realizar test</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.showRiskFactor}>
          <Text style={styles.text}>Índice de riesgo</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.showEducative}>
          <Text style={styles.text}>Módulo Educativo</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={this.showSurvey}>
          <Text style={styles.text}>Cuestionario</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress ={ ()=>{
        console.log('clicked');
        return BackHandler.exitApp();
        }
      }>
          <Text style={styles.text}
          >Salir</Text>
        </TouchableOpacity>
        </View>

      </View>
    </Fragment>;

  }


  // <TouchableOpacity
  //   style={styles.button}
  //   onPress={this.showAdmin}
  // >
  //   <Text style={styles.text}>Administrador</Text>
  // </TouchableOpacity>
  //

  renderRealtimeDemo() {
    return <Fragment>
      <RealtimeDemo returnToMain={this.showMainScreen}/>
    </Fragment>;
  }
  renderTests() {
    return <Fragment>
      <Tests returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderAdmin() {
    return <Fragment>
      <Administrador returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderRiskFactor() {
    return <Fragment>
      <RiskFactor returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderTestResult() {
    return <Fragment>
      <TestResult returnToMain={this.showTestResult}/>
    </Fragment>;
  }
  renderSurvey() {
    return <Fragment>
      <Survey returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderEducative() {
    return <Fragment>
      <Educative returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderWebcamDemo() {
    return <Fragment>
      <WebcamDemo returnToMain={this.showMainScreen}/>
    </Fragment>;
  }

  renderLoadingTF() {
    return <Fragment>
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionTitle}>Loading TF</Text>
      </View>
    </Fragment>;
  }

  renderRealtimePlantilla() {
      return <Fragment>
        <RealtimePlantilla returnToMain={this.showMainScreen}/>
      </Fragment>;
  }

  renderContent() {
    const { currentScreen, isTfReady } = this.state;
    if (isTfReady) {
      switch (currentScreen) {
        case 'main':
          return this.renderMainScreen();
         case 'diag':
           return this.renderDiagnosticScreen();
        case 'realtime':
          return this.renderRealtimeDemo();
        case 'tests':
          return this.renderTests();
        case 'riskFactor':
          return this.renderRiskFactor();
        case 'testResult':
          return this.renderTestResult();
        case 'survey':
          return this.renderSurvey();
        case 'educative':
          return this.renderEducative();
        case 'body':
          return this.renderBodyrender();
        case 'realtime':
          return this.renderRealtimeDemo();
        case 'realtimeplantilla':
          return this.renderRealtimePlantilla();
        case 'admin':
          return this.renderAdmin();
        default:
          return this.renderMainScreen();
      }
    } else {
      return this.renderLoadingTF();
    }

  }

  render() {
    return (
      <Fragment>
      <StatusBar barStyle='dark-content' />
      <SafeAreaView>
        <View style={styles.body}>
          {this.renderContent()}
        </View>
      </SafeAreaView>
    </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    backgroundColor: "#00aaff",
    padding: '3%',
    margin: '3%',
    borderRadius: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#00aaff',
    padding: '3%',
    margin: '3%',
    borderRadius: 10,
  },
  scrollView: {
    backgroundColor: 'white',
  },
  body: {
    backgroundColor: 'white',
  },
  sectionContainer: {
    // width: '100%',
    // height: '100%',
    marginTop: 10,
    paddingHorizontal: 24,
    paddingVertical: 10,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: '600',
    color: 'black',
    marginBottom: 6,
    marginTop: 6
  },
  bodyText: {
    fontSize: 30,
  },
  titleText: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
  },
  text: {
    textAlign: 'center',
    alignSelf: "center",
      alignItems: "center",
      color: '#FFFFFF',
      fontSize: 22,
  },
  textButton: {
    color: "#FFFFFF",
    fontSize: 20,
    justifyContent: "center",
    alignItems: "center",

},
  textModal: {
    color: 'black',
    fontSize: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  }
});
